package blueprint

import (
	"errors"
	"fmt"
	"log"
	"regexp"
)

// City holds the city blueprint of static structures.
//
// The blueprint is memorized as a matrix of Rows x Cols: the first
// index represents the row, the second index represents the coloumn.
//
// Moreover the matrix has a default direction:
// - the first row (Map[0][:]) is the North edge of the city
// - the last coloumn (Map[:][Cols-1]) is the East edge of the city
// - the last row (Map[Rows-1][:]) is the South edge of the city
// - the first coloumn (Map[:][0]) is the West edge of the city
type City struct {
	Rows int
	Cols int
	Map  [][]string
}

// NewCity returns a CityBlueprint with a Map of the specified sizes.
func NewCity(rows, cols int) *City {
	outer := make([][]string, rows)
	inner := make([]string, rows*cols)
	for k := 0; k < rows; k++ {
		outer[k] = inner[k*cols : (k+1)*cols]
	}
	return &City{Rows: rows, Cols: cols, Map: outer}
}

// Fill fills the Map with the supplied value
func (v *City) Fill(value string) {
	for i := 0; i < v.Rows*v.Cols; i++ {
		v.Map[i/v.Cols][i%v.Cols] = value
	}
}

// AreCoordinatesValid check if the supplied coordinates are out-of-bound.
func (v *City) AreCoordinatesValid(row, col int) bool {
	inRange := func(value, min, max int) bool {
		return value >= min && value < max
	}
	return inRange(row, 0, v.Rows) && inRange(col, 0, v.Cols)
}

// At retrieve the structure at the supplied coordinate.
func (v *City) At(row, col int) string {
	if v.AreCoordinatesValid(row, col) {
		return v.Map[row][col]
	}
	return v.Map[0][0]
}

// Move returns the Coordinate that lies in the specified direction wrt `location`.
// If the resulting coordinate lies outside the CityBlueprint, an error is returned.
func (v *City) Move(location Coord, direction int) (Coord, error) {
	result := location.Go(Dir(direction), 1)
	if v.AreCoordinatesValid(result.Row, result.Col) {
		return result, nil
	}
	return result, errors.New("out of border")
}

// FollowDirection move vector one step forward wrt the map.
// If the resulting vector lies outside the CityBluepring, an error is returned.
func (v *City) FollowDirection(vector OrientedCoord) (OrientedCoord, error) {
	c, err := v.Move(vector.Position, int(vector.Direction))
	return OrientedCoord{c, vector.Direction}, err
}

// AfterOrientation tells where the supplied coordinate will fall after
// the City.Map has been rotated to have the supplied direction as the
// vertical cardinal point.
// AfterOrientation is the inverse of BeforeOrientation =>
// AfterOrientation(BeforeOrientaion(pos, dir), dir) = pos
func (v *City) AfterOrientation(pos Coord, dir Dir) Coord {
	switch dir {
	case North:
		return pos
	case East:
		return Coord{v.Cols - pos.Col - 1, pos.Row}
	case South:
		return Coord{v.Rows - pos.Row - 1, v.Cols - pos.Col - 1}
	case West:
		return Coord{pos.Col, v.Rows - pos.Row - 1}
	}
	log.Printf("city.AfterOrientation(%v, %v): Unknown direction", pos, dir)
	return pos
}

// BeforeOrientation tells what is the coordinate of City.Map given
// that the supplied coordinate refers to a map where the supplied
// direction is the current vertical cardinal point.
// Aka:
// BeforeOrientation is the inverse of AfterOrientation =>
// BeforeOrientation(AfterOrientation(pos, dir), dir) = pos
func (v *City) BeforeOrientation(pos Coord, dir Dir) Coord {
	switch dir {
	case North:
		return pos
	case East:
		return Coord{pos.Col, v.Rows - pos.Row - 1}
	case South:
		return Coord{v.Rows - pos.Row - 1, v.Cols - pos.Col - 1}
	case West:
		return Coord{v.Cols - pos.Col - 1, pos.Row}
	}
	log.Printf("city.BeforeOrientation(%v, %v): Unknown direction", pos, dir)
	return pos
}

// Contains checks if the city at the supplied coordinate contains the expected value.
// If the coordinate lies outside the city bountaries, it defaults to false.
// Each element in `lookingFor` is treated as a RegExp
func (v *City) Contains(i, j int, lookingFor []string) bool {
	if v.AreCoordinatesValid(i, j) {
		element := v.At(i, j)
		for _, target := range lookingFor {
			if match, _ := regexp.MatchString(target, element); match {
				return true
			}
		}
	}
	return false
}

// FindNextTileNear finds the nearest tile containing any of the values of `lookingFor` while
// starting at the tile (i,j) and proceding in a spiral way.
// `radius` is the last concentric square-crown to be searched, where `radius==0` identifies
//  just the supplied tile (i, j), `radius==1` identifies all tiles of the 3x3 square centerd
// in (i,j), `radius==2` the square 5x5 centered in (i,j) etc. Negative values are ignored.
// It returns if the search was successful and:
// - the indexes of the first-encountered tile matching the supplied  specifications
// - the indexes of the last inspected tile before abandoning the search if unsuccessful
func (v *City) FindNextTileNear(i, j int, lookingFor []string, radius int) (int, int, bool) {
	if v.Contains(i, j, lookingFor) {
		return i, j, true
	}
	checkStride := func(a, b, strideLenght int, nextCoordinates func(int, int) (int, int)) (int, int, bool) {
		for k := 0; k < strideLenght; k++ {
			a, b = nextCoordinates(a, b)
			if v.Contains(a, b, lookingFor) {
				return a, b, true
			}
		}
		return a, b, false
	}
	goNorth := func(a, b int) (int, int) { return a - 1, b }
	goSouth := func(a, b int) (int, int) { return a + 1, b }
	goEast := func(a, b int) (int, int) { return a, b + 1 }
	goWest := func(a, b int) (int, int) { return a, b - 1 }
	goAround := []func(int, int) (int, int){goNorth, goWest, goSouth, goEast}
	found := false
	for crown := 1; crown <= radius; crown++ {
		// start inspection from the bottom right corner of the square-crown
		i, j = goSouth(goEast(i, j))
		for _, movement := range goAround {
			i, j, found = checkStride(i, j, crown*2, movement)
			if found {
				return i, j, true
			}
		}
	}

	return i, j, false
}

func (v *City) IsTileBetween(i, j int, lookingFor []string, horizontal, vertical bool) bool {
	betweenHorizontal := false
	betweenVertical := false
	if horizontal {
		betweenHorizontal = v.Contains(i, j-1, lookingFor) && v.Contains(i, j+1, lookingFor)
	}
	if vertical {
		betweenVertical = v.Contains(i-1, j, lookingFor) && v.Contains(i+1, j, lookingFor)
	}
	return betweenHorizontal || betweenVertical
}

func (v *City) ToString() string {
	result := ""
	for i := 0; i < v.Rows; i++ {
		for j := 0; j < v.Rows; j++ {
			result = fmt.Sprintf("%s %8s", result, v.Map[i][j])
		}
		result = fmt.Sprintf("%s\n", result)
	}
	return result
}
