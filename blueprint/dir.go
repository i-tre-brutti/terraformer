package blueprint

import "log"

// Dir represents a cardinal direction.
// Dir is tightly coupled with the representation of City:
//
//    North
// W +-----+ E                     	W	  /\	N
// e |     | a   == isometric  ==> 		/    \
// s |     | s      projection     		\    /
// t +-----+ t                     	S	  \/	E
//    South
type Dir int

const (
	North Dir = 0
	East      = 1
	South     = 2
	West      = 3
)

func AllDirs() []Dir {
	return []Dir{North, East, South, West}
}

func (v Dir) ToString() string {
	switch v {
	case North:
		return "North"
	case East:
		return "East"
	case South:
		return "South"
	case West:
		return "West"
	}
	return "North"
}

// DirFromString creates a Dir from its string representation.
// Decide whether to drop one of either representation and keep only
// one of either "digit" or "letter"
func DirFromString(d string) Dir {
	switch d {
	case "N":
		fallthrough
	case "0":
		return North
	case "E":
		fallthrough
	case "1":
		return East
	case "S":
		fallthrough
	case "2":
		return South
	case "W":
		fallthrough
	case "3":
		return West
	// compatibility layer: should be dropped because bugged
	case "NE":
		return West
	case "SE":
		return South
	case "SW":
		return East
	case "NW":
		return North
	}
	log.Printf("DirFromString received an unknow direction: %v", d)
	return North
}

// RotateOn tells where the old direction falls given the newNorth
// as the vertical cardinal point.
func (v Dir) RotateOn(newNorth Dir) Dir {
	return Dir((4 + int(v) - int(newNorth)) % 4)
}
