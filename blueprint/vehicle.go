package blueprint

import (
	"strings"
)

type Vehicle struct {
	ID            string
	MovingVector  OrientedCoord
	Offset        float32
	CurrentSpeed  float32
	OriginalSpeed float32
}

func NewVehicle(name string, startVector OrientedCoord, speed float32) Vehicle {
	return Vehicle{
		ID:            name,
		MovingVector:  startVector,
		Offset:        0.0,
		CurrentSpeed:  speed,
		OriginalSpeed: speed,
	}
}

func (v *Vehicle) Advance(delta float32) {
	v.Offset = v.Offset + v.CurrentSpeed*delta
}

func (v *Vehicle) RepositionOnTile(newVector OrientedCoord) {
	v.MovingVector = newVector
	v.Offset = 0.0
}

func (v *Vehicle) IsInsideTile() bool {
	return v.Offset < 1.0
}

func (v *Vehicle) ChangeDirection(newDirection Dir) {
	if newDirection == v.MovingVector.Direction {
		return
	}
	var afterTurnOffset float32
	switch newDirection {
	case North:
		afterTurnOffset = 0.2
	case East:
		afterTurnOffset = 1.0
	case South:
		afterTurnOffset = 0.8
	case West:
		afterTurnOffset = 0.5
	}
	v.ID = strings.Replace(v.ID, v.MovingVector.Direction.ToString(), newDirection.ToString(), -1)
	v.MovingVector.Direction = newDirection
	v.Offset = afterTurnOffset
	v.CurrentSpeed = v.OriginalSpeed
}

func (v *Vehicle) ViewedFromOrientation(currentOrientation Dir, remapping func(Coord, Dir) Coord) Vehicle {

	newOrientation := v.MovingVector.Direction.RotateOn(currentOrientation)
	newPosition := remapping(v.MovingVector.Position, currentOrientation)
	newVector := OrientedCoord{newPosition, newOrientation}
	newName := strings.Replace(v.ID, v.MovingVector.Direction.ToString(), newOrientation.ToString(), -1)
	result := NewVehicle(newName, newVector, 0)

	result.Offset = v.Offset
	result.CurrentSpeed = v.CurrentSpeed
	result.OriginalSpeed = v.OriginalSpeed
	return result
}

func (v *Vehicle) UpdateSpeed(newSpeed float32) {
	v.CurrentSpeed = newSpeed
}
