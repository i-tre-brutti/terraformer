package blueprint

// Coord represents the indexes used to access City.Map
type Coord struct {
	Row int
	Col int
}

// NewCoord creates a new Coord.
func NewCoord(row, col int) Coord {
	return Coord{Row: row, Col: col}
}

// GetCoords returns the index of the row and col relative to v (it is a shortcut).
func (c Coord) GetCoords() (int, int) {
	return c.Row, c.Col
}

// Go makes n step in the specified direction and returns where
// the coordinate will fall.
func (c Coord) Go(dir Dir, n int) Coord {
	row, col := c.GetCoords()
	switch dir {
	case North:
		return NewCoord(row-1, col)
	case East:
		return NewCoord(row, col+1)
	case South:
		return NewCoord(row+1, col)
	case West:
		return NewCoord(row, col-1)
	}
	panic("Unknown direction")
}

// OrientedCoord represents a point in City.Map facing a specific
// cardinal point
type OrientedCoord struct {
	Position  Coord
	Direction Dir
}

// GetCoords returns the index of the row and col relative to v (it is a shortcut).
func (v OrientedCoord) GetCoords() (int, int) {
	return v.Position.Row, v.Position.Col
}
