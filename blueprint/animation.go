package blueprint

type Animation struct {
	ID       string
	Progress float32
	Location Coord
}
