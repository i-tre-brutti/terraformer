package blueprint

import (
	"testing"
)

func TestAfterOrientaiton(t *testing.T) {
	c := NewCity(10, 10)

	pos := Coord{2, 1}
	expectedPosAfter := map[Dir]Coord{
		North: Coord{2, 1},
		East:  Coord{8, 2},
		South: Coord{7, 8},
		West:  Coord{1, 7},
	}

	for _, dir := range AllDirs() {
		if c.AfterOrientation(pos, dir) != expectedPosAfter[dir] {
			t.Errorf(
				"AfterRotation(%v, %v): got %v expected %v",
				dir.ToString(),
				pos,
				c.AfterOrientation(pos, dir),
				expectedPosAfter[dir])
		}
		if c.AfterOrientation(c.BeforeOrientation(pos, dir), dir) != pos {
			t.Errorf("AfterOrientation is not the inverse of BeforeOrientation for dir %v", dir.ToString())
		}
	}
}

func TestBeforeOrientaiton(t *testing.T) {
	c := NewCity(10, 10)

	pos := Coord{2, 1}
	expectedPosBefore := map[Dir]Coord{
		North: Coord{2, 1},
		East:  Coord{1, 7},
		South: Coord{7, 8},
		West:  Coord{8, 2},
	}

	for _, dir := range AllDirs() {
		if c.BeforeOrientation(pos, dir) != expectedPosBefore[dir] {
			t.Errorf(
				"BeforeRotation(%v, %v): got %v expected %v",
				dir.ToString(),
				pos,
				c.BeforeOrientation(pos, dir),
				expectedPosBefore[dir])
		}
		if c.BeforeOrientation(c.AfterOrientation(pos, dir), dir) != pos {
			t.Errorf("BeforeOrientation is not the inverse of AfterOrientation for dir %v", dir.ToString())
		}
	}

}

func TestIdempotentOrientation(t *testing.T) {
	c := NewCity(10, 10)

	pos := Coord{2, 1}
	p1 := c.AfterOrientation(pos, East)
	p1 = c.AfterOrientation(p1, East)
	p1 = c.AfterOrientation(p1, East)
	p2 := c.AfterOrientation(pos, West)
	if p1 != p2 {
		t.Error("AAAA ", p1, p2)
	}
}

func TestRemappingOrientationRectangleCity(t *testing.T) {
	c := NewCity(10, 20)

	pos := Coord{1, 19}
	posAfter := Coord{0, 1}
	posBefore := Coord{19, 8}

	if c.BeforeOrientation(pos, East) != posBefore {
		t.Errorf("Wrong remapping for rectangle city: %v instead of %v",
			c.BeforeOrientation(pos, East), posBefore)
	}
	if c.AfterOrientation(pos, East) != posAfter {
		t.Errorf("Wrong remapping for rectangle city: %v instead of %v",
			c.AfterOrientation(pos, East), posAfter)
	}
}

func TestFindNextTileNear(t *testing.T) {
	c := NewCity(5, 5)
	i, j, found := 0, 0, false
	/*
	 Map:
	 - - - 1 -
	 - 0 - - -
	 - 1 - - -
	 - - - - 3
	 - - - - 2
	*/
	c.Fill("-")
	c.Map[1][1] = "0"
	c.Map[0][3] = "1"
	c.Map[2][1] = "1"
	c.Map[4][4] = "2"
	c.Map[3][4] = "3"

	tables := []struct {
		originI       int
		originJ       int
		radius        int
		pattern       string
		expectedI     int
		expectedJ     int
		expectedFound bool
	}{
		// looking for element in the center
		{1, 1, 0, "0", 1, 1, true},
		{1, 1, 1, "0", 1, 1, true},
		{1, 1, 2, "0", 1, 1, true},
		{1, 1, 3, "0", 1, 1, true},
		// lookgin for element available multiple time
		{1, 1, 0, "1", 1, 1, false},
		{1, 1, 1, "1", 2, 1, true},
		{1, 1, 2, "1", 2, 1, true},
		{1, 1, 3, "1", 2, 1, true},
		// lookgin for a far-away element
		{1, 1, 0, "2", 1, 1, false},
		{1, 1, 1, "2", 2, 2, false},
		{1, 1, 2, "2", 3, 3, false},
		{1, 1, 3, "2", 4, 4, true},
		// lookgin for a regexp
		{1, 1, 0, "(2|3)", 1, 1, false},
		{1, 1, 1, "(2|3)", 2, 2, false},
		{1, 1, 2, "(2|3)", 3, 3, false},
		{1, 1, 3, "(2|3)", 3, 4, true},
	}

	for _, table := range tables {
		i, j, found = c.FindNextTileNear(table.originI, table.originJ, []string{table.pattern}, table.radius)
		if i != table.expectedI || j != table.expectedJ || found != table.expectedFound {
			t.Errorf("Origin(%d, %d) & radius = %d for pattern %s failed the test: expected %b@(%d, %d), found %b(%d, %d)",
				table.originI, table.originJ, table.radius, table.pattern,
				table.expectedFound, table.expectedI, table.expectedJ,
				found, i, j)
		}
	}

}
