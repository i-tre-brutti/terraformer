package loop

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
)

func (l *mainLoop) render() {
	l.updateMatrix()
	l.drawHUD()
	l.drawCompass()
	l.syncFPS()
	l.countedFrames++

	l.renderer.Present()
	// background
	l.renderer.SetDrawColor(167, 125, 83, 255)
	l.renderer.Clear()
}

var compassCache *sdl.Surface

func (l *mainLoop) drawCompass() {
	var width int32 = 200
	var height int32 = 100
	assetsPath, _ := filepath.Abs("./assets")
	path := assetsPath + "/images/compass.png"

	if compassCache == nil {
		img, err := img.Load(path)
		utils.E(err, 0)
		compassCache = img
	}

	src := sdl.Rect{X: int32(l.camera.Orientation) * 200, Y: 0, W: width, H: height}
	dst := sdl.Rect{X: 0, Y: 0, W: width, H: height}
	img, err := sdl.CreateRGBSurface(0, width, height, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff)
	utils.E(err, 0)
	defer img.Free()
	compassCache.Blit(&src, img, &dst)
	imgTexture, err := l.renderer.CreateTextureFromSurface(img)
	utils.E(err, 0)
	defer imgTexture.Destroy()

	finalDest := sdl.Rect{X: 20, Y: 20, W: width, H: height}
	err = l.renderer.Copy(imgTexture, &dst, &finalDest)
	utils.E(err, 0)
}

func (l *mainLoop) updateMatrix() {
	drawer.Orientation = l.camera.Orientation
	for i := 0; i < l.state.city.Rows; i++ {
		for j := 0; j < l.state.city.Cols; j++ {
			mapI, mapJ := drawer.AfterRotation(i, j)
			key := drawer.RemapOrientation(l.state.city.At(mapI, mapJ))
			if isTerrain(key) {
				drawer.RenderTile(l.renderer, &l.camera, i, j, key)
				l.drawVehicle(&l.camera, i, j)
			} else if isBuilding(key) {
				drawer.RenderTile(l.renderer, &l.camera, i, j, key)
				l.drawAnimation(&l.camera, i, j)
			}
		}
	}
}

func isTerrain(ID string) bool {
	parts := strings.Split(ID, ":")
	return parts[0] == "T" || parts[0] == "D"
}

func isBuilding(ID string) bool {
	parts := strings.Split(ID, ":")
	return parts[0] == "B"
}

func (l *mainLoop) drawVehicle(camera *drawer.CameraState, i, j int) {
	// order to consider to avoid strange overlapping
	directions := []blueprint.Dir{
		blueprint.South,
		blueprint.North,
		blueprint.West,
		blueprint.East}
	vehiclesToDraw := make(map[blueprint.Dir]blueprint.Vehicle)
	mapI, mapJ := drawer.AfterRotation(i, j)
	for _, d := range blueprint.AllDirs() {
		mapkey := blueprint.OrientedCoord{blueprint.Coord{mapI, mapJ}, d}
		v, ok := l.state.vehicles[mapkey]
		if ok {
			remappedVehicle := v.ViewedFromOrientation(camera.Orientation, l.state.city.AfterOrientation)
			vehiclesToDraw[d] = remappedVehicle
		}
	}
	for _, d := range directions {
		v, isPresent := vehiclesToDraw[d]
		if isPresent {
			// drawer.RenderTile(l.renderer, i, j, "T:0:0:0")
			drawer.RenderVehicle(l.renderer, camera, i, j, &v)
		}
	}
}

func (l *mainLoop) drawAnimation(camera *drawer.CameraState, i, j int) {
	mapI, mapJ := drawer.AfterRotation(i, j)
	remappedLocation := blueprint.Coord{mapI, mapJ}
	for _, v := range l.state.events {
		if v.Location == remappedLocation {
			drawer.RenderAnimation(l.renderer, camera, i, j, v)
		}
	}
}

var lastTimestamp time.Time
var lastFPS int
var countSinceLast int

func (l *mainLoop) syncFPS() {
	tick := time.Now()
	elapsedMS := float64(tick.Sub(l.timer)) / float64(time.Millisecond)
	if utils.IsDebugEnv() {
		var zeroTime time.Time
		if lastTimestamp == zeroTime {
			lastTimestamp = time.Now()
		} else {
			if time.Since(lastTimestamp) < time.Duration(time.Second) {
				countSinceLast++
			} else {
				lastTimestamp = time.Now()
				if os.Getenv("FPSLOG") == "1" {
					log.Printf("[%v] update FPS: %v -> %v\n", lastTimestamp, lastFPS, countSinceLast+1)
				}
				lastFPS = countSinceLast + 1
				countSinceLast = 0
			}
			l.drawFPS(fmt.Sprintf("%v", lastFPS))
		}
	}
	if sleep := TICKSPERFRAME - elapsedMS; sleep > 0 {
		d := time.Duration(sleep)
		sdl.Delay(uint32(d))
	}
}

func (l *mainLoop) drawFPS(text string) error {
	fontColor := sdl.Color{R: 0, G: 0, B: 0, A: 0}
	textSurface, err := font.RenderUTF8_Blended(fmt.Sprintf("%s FPS", text), fontColor)
	if err != nil {
		return err
	}
	defer textSurface.Free()

	textTexture, err := l.renderer.CreateTextureFromSurface(textSurface)
	if err != nil {
		return err
	}
	defer textTexture.Destroy()

	src := sdl.Rect{X: 0, Y: 0, W: textSurface.W, H: textSurface.H}
	dest := sdl.Rect{X: 0, Y: 0, W: textSurface.W, H: textSurface.H}
	l.renderer.Copy(textTexture, &src, &dest)

	return nil
}
