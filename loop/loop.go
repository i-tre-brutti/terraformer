package loop

import (
	"flag"
	"log"
	"time"

	"github.com/veandco/go-sdl2/mix"
	"gopkg.in/mgo.v2/bson"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
)

// Run makes the app do its magic
func Run() {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	utils.E(err, 0)
	defer sdl.Quit()

	initSDL()

	// Initialize music
	err = mix.Init(mix.INIT_OGG)
	if err != nil {
		log.Println(err)
		// Great, if you ignore the error (don't panic), it works :)
		// panic(err)
	}
	err = mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096)
	if err != nil {
		panic(err)
	}

	defer mix.Quit()
	music, err := mix.LoadMUS("./assets/audio/soundtrack.ogg")
	if err != nil {
		panic(err)
	}
	defer music.Free()
	music.Play(-1)

	projectID := flag.String("P", "", "Project ID")
	fullScreen := flag.Bool("F", false, "Run in fullscreen mode")
	flag.Parse()

	l := mainLoop{isRunning: true, arguments: commandLineArguments{fullScreen: *fullScreen, projectID: *projectID}}
	l.camera.Reset()
	initTerraformer(&l.state, &l.arguments)

	// poll for events
	events = make(chan LiveEvent)
	go func() {
		c := time.Tick(5 * time.Second)
		for range c {
			pollEvents(l.state.project.projectID)
		}
	}()
	go func() {
		for event := range events {
			currentEvent = hudEvent{event: event, timestamp: time.Now(), isInvalid: false}
			time.Sleep(5 * time.Second)
		}
	}()
	defer close(events)

	l.createWindow()
	defer l.window.Destroy()

	l.createRenderer()
	defer l.renderer.Destroy()

	l.timer = time.Now()
	// Main loop
	for l.isRunning {
		l.previousTimer = l.timer
		l.timer = time.Now()
		l.manageInput()
		l.updateStatus()
		l.render()
	}
}

func (e *hudEvent) isValid() bool {
	return !e.isInvalid && time.Now().Sub(e.timestamp) < 1*time.Minute
}

func invalidateCurrentEvent() {
	currentEvent.isInvalid = true
}

func (l *mainLoop) createWindow() {
	var err error
	l.window, err = sdl.CreateWindow(
		"GamingCityFication",
		sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		drawer.GetWindow().Width,
		drawer.GetWindow().Height,
		sdl.WINDOW_SHOWN,
	)
	utils.E(err, 0)
	if l.arguments.fullScreen {
		l.window.SetFullscreen(sdl.WINDOW_FULLSCREEN_DESKTOP)
	}
}

func (l *mainLoop) createRenderer() {
	var err error
	l.renderer, err = sdl.CreateRenderer(l.window, -1, sdl.RENDERER_ACCELERATED)
	utils.E(err, 0)

	l.renderer.SetDrawColor(0, 255, 0, 255)
	l.renderer.Clear()
}

func pollEvents(projectID string) {
	log.Println("Polling for events")
	db.InitSession()
	defer db.CloseSession()
	var newEvent LiveEvent
	err := db.GetLiveEventsCollection().Find(bson.M{"project_id": projectID}).Sort("-timestamp").One(&newEvent)
	if err != nil && err.Error() != "not found" {
		panic(err)
	}
	if err == nil && newEvent.Timestamp != currentEvent.event.Timestamp {
		events <- newEvent
	}
}
