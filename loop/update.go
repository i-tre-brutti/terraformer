package loop

import (
	"math/rand"
	"time"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"

	"gitlab.com/i-tre-brutti/terraformer/vehicles"
)

func (l *mainLoop) updateStatus() {
	l.manageVehicles()
	l.addNewEvents()
	l.progressEvents()
}

func (l *mainLoop) manageVehicles() {
	if l.state.vehicles == nil {
		l.state.vehicles = make(map[blueprint.OrientedCoord]*blueprint.Vehicle)
	}
	vehicles.SpawnVehicles(l.state.city, &l.state.vehicles, l.state.numberOfVehicles)
	delta := float32(l.timer.Sub(l.previousTimer)/time.Millisecond) / 1000
	vehicles.MoveVehicles(delta, l.state.city, &l.state.vehicles)
}

func (l *mainLoop) addNewEvents() {
	if l.state.events == nil {
		l.state.events = make(map[hudEvent]*blueprint.Animation)
	}
	if !currentEvent.isValid() {
		// expired event
		return
	}
	if _, ok := l.state.events[currentEvent]; ok {
		// already added event
		return
	}
	// Here logic to create an animation from an event
	if currentEvent.event.EventType == "build_failure" {
		newAnimation := blueprint.Animation{}
		newAnimation.ID = "A::0:0"
		newAnimation.Progress = 1.1 // will be reset in `progreeEvents`!
		emptyLocationFound := false
		for k := 0; !emptyLocationFound || k < 20; k++ {
			i, j := rand.Intn(l.state.city.Rows), rand.Intn(l.state.city.Cols)
			i, j, found := l.state.city.FindNextTileNear(i, j, []string{"B:"}, 10)
			if found {
				newAnimation.Location = blueprint.Coord{i, j}
				emptyLocationFound = true
				for _, v := range l.state.events {
					if v.Location == newAnimation.Location {
						emptyLocationFound = false
						break
					}
				}
			}
		}
		// add new event only if a new empty location has been found
		if emptyLocationFound {
			l.state.events[currentEvent] = &newAnimation
		}
	}
}

func (l *mainLoop) progressEvents() {
	for i, v := range l.state.events {
		if !i.isValid() {
			delete(l.state.events, i)
		} else {
			v.Progress += 0.02
			if v.Progress > 1.0 {
				v.Progress = 0.0
			}
		}
	}
}
