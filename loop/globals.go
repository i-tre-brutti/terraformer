package loop

import (
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
)

// FPS are the frame per seconds target
const FPS = 60

// TICKSPERFRAME is the number of ticks required to reach FPS
const TICKSPERFRAME = 1000.0 / FPS

type currentProject struct {
	projectID   string
	projectName string
	updatedAt   time.Time
}
type mainState struct {
	project          currentProject
	city             *blueprint.City
	stats            interfaces.ProjectStatistics
	vehicles         map[blueprint.OrientedCoord]*blueprint.Vehicle
	events           map[hudEvent]*blueprint.Animation
	numberOfVehicles int
}

type commandLineArguments struct {
	fullScreen bool
	projectID  string
}

type mainLoop struct {
	isRunning     bool
	countedFrames uint32
	window        *sdl.Window
	renderer      *sdl.Renderer
	previousTimer time.Time
	timer         time.Time
	state         mainState
	camera        drawer.CameraState
	arguments     commandLineArguments
}

// LiveEvent represents live events
type LiveEvent struct {
	ProjectID string    `json:"project_id" bson:"project_id"`
	EventType string    `json:"event_type" bson:"event_type"`
	ExtraInfo string    `json:"extra_info" bson:"extra_info"`
	Timestamp time.Time `json:"timestamp" bson:"timestamp"`
}

type hudEvent struct {
	timestamp time.Time
	event     LiveEvent
	isInvalid bool
}

var events chan LiveEvent
var currentEvent hudEvent

var font *ttf.Font
var hudFont *ttf.Font

var projectLoopDuration = 60 * time.Second
var availableProjects []string
