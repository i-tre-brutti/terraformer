package loop

import (
	"errors"
	"log"
	"path/filepath"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/builder"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
	"gitlab.com/i-tre-brutti/terraformer/engineer"
	"gopkg.in/mgo.v2/bson"
)

func initSDL() error {
	// Initialize SDL img to load png images
	ret := img.Init(img.INIT_PNG)
	if ret == 0 {
		return errors.New("Can't initize SDL IMG")
	}

	// Initialize TTF
	ttf.Init()
	var err error
	font, err = ttf.OpenFont("./assets/fonts/mono.ttf", 14)
	if err != nil {
		return err
	}
	hudFont, err = ttf.OpenFont("./assets/fonts/hud.ttf", 28)
	if err != nil {
		return err
	}

	return nil
}

func initTerraformer(state *mainState, arguments *commandLineArguments) {
	setProjectID(state, arguments)
	log.Println("Initializing project", state.project.projectID)

	var assetsPath string
	assetsPath, err := filepath.Abs("./assets")
	utils.E(err, 0)

	err = drawer.InitTerraformer(assetsPath, arguments.fullScreen)
	utils.E(err, 0)
	// city = engineer.BuildCity(projectID)
	if state.project.projectID == "rand" {
		state.city, err = engineer.MakeRandomCity(drawer.MatrixSize, drawer.MatrixSize)
	} else if state.project.projectID == "rand2" {
		state.city, err = engineer.MakeAsymmetricCity(drawer.MatrixSize, drawer.MatrixSize)
	} else if state.project.projectID == "cars" {
		state.city, err = engineer.MakeCityDebugCars(0, 0)
		state.numberOfVehicles = 25
	} else {
		initProject(state)
	}
	utils.E(err, 0)
	err = builder.InitCurrentStyles(assetsPath)
	utils.E(err, 0)
}

func initProject(state *mainState) {
	log.Println("Initializing project...")
	stats, _ := engineer.LoadStatistics(state.project.projectID)
	state.stats = stats
	state.project.projectName = stats.ProjectID
	state.city = engineer.BuildCity(stats)
	state.numberOfVehicles = engineer.GetVehiclesNumber(stats)
	invalidateCurrentEvent()
}

func setProjectID(state *mainState, arguments *commandLineArguments) {
	if availableProjects == nil {
		db.InitSession()
		defer db.CloseSession()
		var statistics []interfaces.ProjectStatistics
		err := db.GetStatisticsCollection().Find(bson.M{}).All(&statistics)
		if err != nil {
			panic(err)
		}
		for _, stat := range statistics {
			availableProjects = append(availableProjects, stat.ProjectID)
		}
	}

	if arguments.projectID == "" {
		if len(availableProjects) > 1 {
			// loop over all projects
			_setCurrentProjectID(state, 0)
			go func() {
				index := 1
				c := time.Tick(projectLoopDuration)
				for range c {
					_setCurrentProjectID(state, index%len(availableProjects))
					initProject(state)
					index++
				}
			}()
		} else {
			state.project.projectID = availableProjects[0]
		}
	} else {
		state.project.projectID = arguments.projectID
	}
}

func _setCurrentProjectID(state *mainState, index int) {
	projectID := availableProjects[index]
	log.Println("Loading project", projectID)
	state.project.projectID = projectID
	state.project.updatedAt = time.Now()
}
