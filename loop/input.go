package loop

import "github.com/veandco/go-sdl2/sdl"

// CurrentInput holds the input of the user
type CurrentInput struct {
	moveUp      bool
	moveDown    bool
	moveLeft    bool
	moveRight   bool
	zoomIn      bool
	zoomOut     bool
	rotateLeft  bool
	rotateRight bool
	reset       bool
	quit        bool
}

func (l *mainLoop) manageInput() {
	input := CurrentInput{}

	// read input from keyboard
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			input.quit = true
		case *sdl.KeyDownEvent:
			// useful if we want to manage "key is being pressed"
			break
		case *sdl.KeyUpEvent:
			switch t.Keysym.Scancode {
			case sdl.SCANCODE_ESCAPE:
				input.quit = true
			case sdl.SCANCODE_A:
				input.moveLeft = true
			case sdl.SCANCODE_D:
				input.moveRight = true
			case sdl.SCANCODE_W:
				input.moveUp = true
			case sdl.SCANCODE_S:
				input.moveDown = true
			case sdl.SCANCODE_Q:
				input.rotateLeft = true
			case sdl.SCANCODE_E:
				input.rotateRight = true
			case sdl.SCANCODE_Z:
				input.zoomOut = true
			case sdl.SCANCODE_X:
				input.zoomIn = true
			case sdl.SCANCODE_H:
				input.reset = true
			}
		}
	}

	// sanitize input: avoid conflicting operations
	if input.moveLeft && input.moveRight {
		input.moveLeft = false
		input.moveRight = false
	}
	if input.moveUp && input.moveDown {
		input.moveUp = false
		input.moveDown = false
	}
	if input.rotateLeft && input.rotateRight {
		input.rotateLeft = false
		input.rotateRight = false
	}
	if input.zoomIn && input.zoomOut {
		input.zoomIn = false
		input.zoomOut = false
	}

	// translate input to an action
	// we make this decision after having read the key pressed because
	// some combinations of keys are not valid or because there are some
	// priorities to be enforced.
	if input.quit {
		l.isRunning = false
		return
	}
	if input.reset {
		l.camera.Reset()
		return
	}
	dx := 0
	dy := 0
	modifier := 1
	if input.moveUp {
		dy = 1
	}
	if input.moveDown {
		dy = -1
	}
	l.camera.TranslateV += modifier * dy
	if input.moveRight {
		dx = 1
	}
	if input.moveLeft {
		dx = -1
	}
	l.camera.TranslateH += modifier * dx
	dz := 0.0
	if input.zoomIn && l.camera.ZoomLevel < 5 {
		dz = 0.125
	} else if input.zoomOut && l.camera.ZoomLevel > 0.21 {
		dz = -0.125
	}
	l.camera.ZoomLevel += float32(dz)

	newOrientation := l.camera.Orientation
	if input.rotateLeft {
		newOrientation = (newOrientation + 1) % 4
	} else if input.rotateRight {
		if newOrientation == 0 {
			newOrientation = 4
		}
		newOrientation = newOrientation - 1
	}
	l.camera.Orientation = newOrientation

}
