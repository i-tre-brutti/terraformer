package loop

import (
	"fmt"
	"math"
	"path/filepath"
	"strings"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
)

var starsCache map[string]*sdl.Surface
var hudBarHeight int32 = 50
var eventTypes map[string]string
var scrollingTextWidth int32
var scrollX int32

var maxTitleSpace int32 = 250
var starSideWidth int32 = 30

func (l *mainLoop) drawHUD() {
	l.renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)

	// Initialize the font
	if hudFont == nil {
		var err error
		hudFont, err = ttf.OpenFont("./assets/fonts/hud.ttf", 20)
		if err != nil {
			panic(err)
		}
	}

	l.drawProgressBar()
	l.drawBar()

	var leftPadding int32 = 20
	leftPadding = l.drawProjectTitle(leftPadding)
	leftPadding = l.drawEvent(leftPadding)
	l.drawStars(l.state.stats.Popularity)
}

// Draw a progress bar to show the timeout for project automatic change
func (l *mainLoop) drawProgressBar() {
	l.renderer.SetDrawColor(86, 156, 214, 230)
	var barHeight int32 = 4

	t := float32(time.Now().Sub(l.state.project.updatedAt).Nanoseconds() * 1000.)
	var w float32
	if t > 0 {
		w = float32((float32(drawer.GetWindow().Width) * t) / float32(projectLoopDuration.Nanoseconds()*1000))
	}

	// Draw main bar
	rect := sdl.Rect{
		X: 0,
		Y: 0,
		W: int32(w),
		H: barHeight,
	}
	l.renderer.FillRect(&rect)

	// Draw a darker line on top of the bar
	l.renderer.SetDrawColor(62, 131, 188, 230)
	lineY := drawer.GetWindow().Height + int(barHeight) + 1
	l.renderer.DrawLine(0, lineY, int(w), lineY)

	// Draw shadow
	for i := 0; i < 5; i++ {
		l.renderer.SetDrawColor(0, 0, 0, uint8(50-i*10))
		lineY := int(barHeight) + i
		l.renderer.DrawLine(0, lineY, int(w), lineY)
	}
}

func (l *mainLoop) drawBar() {
	l.renderer.SetDrawColor(86, 156, 214, 230)

	// Draw main bar
	rect := sdl.Rect{
		X: 0,
		Y: int32(drawer.GetWindow().Height) - hudBarHeight,
		W: int32(drawer.GetWindow().Width),
		H: hudBarHeight,
	}
	l.renderer.FillRect(&rect)

	// Draw a darker line on top of the bar
	l.renderer.SetDrawColor(62, 131, 188, 230)
	lineY := drawer.GetWindow().Height - int(hudBarHeight) - 1
	l.renderer.DrawLine(0, lineY, drawer.GetWindow().Width, lineY)

	// Draw shadow
	for i := 0; i < 5; i++ {
		l.renderer.SetDrawColor(0, 0, 0, uint8(50-i*10))
		lineY := drawer.GetWindow().Height - int(hudBarHeight) - 1 - i
		l.renderer.DrawLine(0, lineY, drawer.GetWindow().Width, lineY)
	}
}

func (l *mainLoop) drawStars(popularity float64) {
	points := math.Floor(popularity * 5)
	if (popularity*5)-points >= 0.5 {
		points = points + 0.5
	}
	if starsCache == nil {
		starsCache = make(map[string]*sdl.Surface)
	}
	assetsPath, _ := filepath.Abs("./assets")
	for i := 0; i < 5; i++ {
		starType := "full"
		if float64(i+1)-points == 0.5 {
			starType = "half"
		} else if points < float64(i+1) {
			starType = "empty"
		}
		path := assetsPath + "/images/star-" + starType + "_30.png"

		if _, found := starsCache[path]; !found {
			img, err := img.Load(path)
			utils.E(err, 0)
			starsCache[path] = img
		}
		imgFile, _ := starsCache[path]

		src := sdl.Rect{X: 0, Y: 0, W: starSideWidth, H: starSideWidth}
		img, err := sdl.CreateRGBSurface(0, starSideWidth, starSideWidth, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff)
		utils.E(err, 0)
		defer img.Free()
		imgFile.Blit(&src, img, &src)
		imgTexture, err := l.renderer.CreateTextureFromSurface(img)
		utils.E(err, 0)
		defer imgTexture.Destroy()

		x := int32(drawer.GetWindow().Width) - (int32(5-i) * (starSideWidth + 2)) - 20
		y := int32(drawer.GetWindow().Height) - hudBarHeight/2 - starSideWidth/2
		dst := sdl.Rect{X: x, Y: y, W: starSideWidth, H: starSideWidth}
		err = l.renderer.Copy(imgTexture, &src, &dst)
		utils.E(err, 0)
	}
}

func (l *mainLoop) drawEvent(titleEndWidth int32) int32 {
	// The space occupied by the events is the total minus the space for the title
	maxWidth := int32(drawer.GetWindow().Width) - maxTitleSpace - starSideWidth*5 - 20
	if eventTypes == nil {
		eventTypes = map[string]string{
			"new_star":      "Project starred",
			"new_commit":    "New commit",
			"new_pr":        "Pull-request opened",
			"build_success": "Build success",
			"build_failure": "Build failure",
		}
	}
	if currentEvent.isValid() {
		t := currentEvent.event.Timestamp
		formattedTime := fmt.Sprintf("%02d/%02d/%02d %02d:%02d:%02d",
			t.Day(), t.Month(), t.Year(),
			t.Hour(), t.Minute(), t.Second(),
		)
		text := fmt.Sprintf("LAST EVENT: %s (%s)", eventTypes[currentEvent.event.EventType], formattedTime)
		if currentEvent.event.ExtraInfo != "" {
			text = fmt.Sprintf("%s - %s", text, currentEvent.event.ExtraInfo)
			if len(text) > 73 {
				return l.writeSlidingText(titleEndWidth+20, text, maxWidth)
			}
		}
		return l.writeText(titleEndWidth+20, text, maxWidth)
	}
	return titleEndWidth
}

func (l *mainLoop) drawProjectTitle(leftPadding int32) int32 {
	title := fmt.Sprintf("\"%s\"", strings.ToUpper(l.state.project.projectName))
	return l.writeText(leftPadding, title, maxTitleSpace)
}

func (l *mainLoop) writeSlidingText(leftPadding int32, text string, maxWidth int32) int32 {
	hudFontColor := sdl.Color{R: 255, G: 255, B: 255, A: 0}
	// Draw the text, twice to draw a shadow
	tempTextSurface, _ := hudFont.RenderUTF8_Blended(text, hudFontColor)

	if scrollingTextWidth == 0 {
		scrollingTextWidth = tempTextSurface.W
		scrollX = scrollingTextWidth
		if maxWidth > 0 && scrollX > maxWidth {
			scrollX = maxWidth
		}
	}
	w := scrollingTextWidth + scrollX*(-1)

	scrollX--
	if scrollX < -(tempTextSurface.W) {
		scrollX = scrollingTextWidth
	}

	s := sdl.Rect{X: 0, Y: 0, W: w, H: tempTextSurface.H}
	d := sdl.Rect{X: scrollX, Y: 0, W: tempTextSurface.W, H: tempTextSurface.H}
	textSurface, err := sdl.CreateRGBSurface(0, tempTextSurface.W, tempTextSurface.H, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff)
	utils.E(err, 0)
	tempTextSurface.Blit(&s, textSurface, &d)
	defer tempTextSurface.Free()
	defer textSurface.Free()
	textTexture, _ := l.renderer.CreateTextureFromSurface(textSurface)
	defer textTexture.Destroy()

	textWidth := textSurface.W
	if maxWidth > 0 && textSurface.W > maxWidth {
		textWidth = maxWidth
	}

	yText := int32(drawer.GetWindow().Height) - hudBarHeight/2 - 12
	src := sdl.Rect{X: 0, Y: 0, W: textWidth, H: textSurface.H}
	dest := sdl.Rect{X: leftPadding, Y: yText, W: textWidth, H: textSurface.H}
	l.renderer.Copy(textTexture, &src, &dest)

	return leftPadding + textWidth
}

func (l *mainLoop) writeText(leftPadding int32, text string, maxWidth int32) int32 {
	hudFontColor := sdl.Color{R: 255, G: 255, B: 255, A: 0}
	// Draw the text, twice to draw a shadow
	textSurface, _ := hudFont.RenderUTF8_Blended(text, hudFontColor)
	defer textSurface.Free()
	textTexture, _ := l.renderer.CreateTextureFromSurface(textSurface)
	// Transparency
	// textTexture.SetAlphaMod(200)
	defer textTexture.Destroy()

	textWidth := textSurface.W
	if maxWidth > 0 && textWidth > maxWidth {
		textWidth = maxWidth
	}

	hudBgFontColor := sdl.Color{R: 62, G: 131, B: 188, A: 0}
	textBgSurface, _ := hudFont.RenderUTF8_Blended(text, hudBgFontColor)
	defer textBgSurface.Free()
	textBgTexture, _ := l.renderer.CreateTextureFromSurface(textBgSurface)
	defer textBgTexture.Destroy()

	yText := int32(drawer.GetWindow().Height) - hudBarHeight/2 - 12
	src := sdl.Rect{X: 0, Y: 0, W: textWidth, H: textSurface.H}

	bgDest := sdl.Rect{X: leftPadding + 1, Y: yText + 1, W: textWidth, H: textSurface.H}
	l.renderer.Copy(textBgTexture, &src, &bgDest)

	dest := sdl.Rect{X: leftPadding, Y: yText, W: textWidth, H: textSurface.H}
	l.renderer.Copy(textTexture, &src, &dest)

	return leftPadding + textWidth
}
