package builder

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/utils"
)

/*
The Builder creates the actual image associated with an identifier. The first time an identifier
is requested, builder creates the image, then it caches it for following requests.
Its job is to read the identifier, understand what's in it and build the final image loading
the requested images from the spritesheets.
*/
var currentStyles map[string]map[string]map[string]elementDesc

type elementDesc struct {
	sprites []string
}

// InitCurrentStyles initializes the styles of the buildings reading from the the json file
// and populating the global var currentStyles. If it's called when the var is already initialized,
// it does nothing
func InitCurrentStyles(assetsPath string) error {
	if currentStyles != nil {
		return nil
	}
	// read file and produce a simle map string-interface
	var rawFile map[string]interface{}
	raw, err := ioutil.ReadFile(assetsPath + "/styles.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw, &rawFile)
	if err != nil {
		return err
	}
	// format raw file as global a multilayered map
	// buildingType -> style -> orientation -> elementDesc
	// NOTE: is entry for buildingType useful at all? elementDesc is
	// designed explicitly for the building only (e.g., not terrain)
	currentStyles = make(map[string]map[string]map[string]elementDesc)
	for tileType, value := range rawFile {
		currentStyles[tileType] = make(map[string]map[string]elementDesc)
		tileStyles := value.(map[string]interface{})
		for style, value2 := range tileStyles {
			currentStyles[tileType][style] = make(map[string]elementDesc)
			tileOrientations := value2.(map[string]interface{})
			for orientation, value3 := range tileOrientations {
				// FIXME: hack to avoid the description data inside the
				// json. Waiting for a better formatted file & data struct.
				if orientation == "title" {
					continue
				}
				var element elementDesc
				element.sprites = make([]string, len(value3.([]interface{})))
				for i, v := range value3.([]interface{}) {
					element.sprites[i] = v.(string)
				}
				currentStyles[tileType][style][orientation] = element
			}
		}
	}

	return nil
}

// BuildStep represents a single instruction to build a part of a building from a spritesheet
type BuildStep struct {
	Spritesheet SpriteSheet
	Rect        sdl.Rect
}

// SpriteSheet has the instruction to retrieve a spritesheet file (image and desc)
type SpriteSheet struct {
	Layer string
	Tile  string
}

// BuildInstructions represents the whole instructions to build a building
type BuildInstructions struct {
	ID    string
	Steps []BuildStep
}

var cache map[string]BuildInstructions

// GetElement returns a building for the given ID
func GetElement(ID string) BuildInstructions {
	if cache == nil {
		log.Println("Initializing buildings cache...")
		cache = make(map[string]BuildInstructions)
	}
	// No building in the cache, build it
	if _, ok := cache[ID]; !ok {
		// Create the building in the cache
		createElement(ID)
	}
	return cache[ID]
}

// Actually calculate the build instructions and saves it in the cache
func createElement(ID string) {
	switch string(ID[0]) {
	case "B":
		// "B:<height>:<style>:<orientation>"
		cache[ID] = buildMultiFloorsBuilding(ID)
	case "T":
		// "T::<style>:<orientation>"
		cache[ID] = buildTerrain(ID)
	case "C":
		// "C:<style>_<orientation>.png"
		cache[ID] = buildCar(ID)
	case "D":
		// "D::<style>:<orientation>"
		cache[ID] = buildDecoration(ID)
	case "A":
		// "A::<style>:0"
		cache[ID] = buildAnimation(ID)
	default:
		cache[ID] = buildTerrain("T::0:0")
	}
}

func buildCar(ID string) BuildInstructions {
	instructions := BuildInstructions{ID: ID}
	parts := strings.Split(ID, ":")

	step := getStepInstructions("cars", parts[1])

	instructions.Steps = append(instructions.Steps, step)

	return instructions
}

func buildTerrain(ID string) BuildInstructions {
	instructions := BuildInstructions{ID: ID}

	parts := strings.Split(ID, ":")
	terrainDesc := currentStyles["T"][parts[2]][parts[3]]
	step := getStepInstructions("landscapes", terrainDesc.sprites[0])

	instructions.Steps = append(instructions.Steps, step)

	return instructions
}

func buildDecoration(ID string) BuildInstructions {
	instructions := BuildInstructions{ID: ID}

	parts := strings.Split(ID, ":")
	decorationDesc := currentStyles["D"][parts[2]][parts[3]]
	step := getStepInstructions("city", decorationDesc.sprites[0])

	instructions.Steps = append(instructions.Steps, step)

	return instructions
}

func buildMultiFloorsBuilding(ID string) BuildInstructions {
	instructions := BuildInstructions{ID: ID}
	parts := strings.Split(ID, ":")

	floorsNum, e := strconv.Atoi(parts[1])
	utils.E(e, 0)

	buildingDesc := currentStyles[parts[0]][parts[2]][parts[3]]

	// floorNum refer only to the "middle" floor. Basement and
	// roof are not considered in this number and as such use use `+2`.
	for i := 0; i < floorsNum+2; i++ {
		var key string
		if i == 0 {
			key = buildingDesc.sprites[0]
		} else if i == floorsNum+1 {
			key = buildingDesc.sprites[2]
		} else {
			key = buildingDesc.sprites[1]
		}

		step := getStepInstructions("buildings", key)
		instructions.Steps = append(instructions.Steps, step)
	}

	return instructions
}

func buildAnimation(ID string) BuildInstructions {
	instructions := BuildInstructions{ID: ID}
	parts := strings.Split(ID, ":")
	animationDesc := currentStyles["A"][parts[2]]["0"]

	for _, v := range animationDesc.sprites {
		step := getStepInstructions("animations", v)
		instructions.Steps = append(instructions.Steps, step)
	}

	return instructions
}

/*
 * UTILS
 */

func getStepInstructions(layerName, tileName string) BuildStep {
	var step BuildStep
	step.Spritesheet = SpriteSheet{Layer: layerName, Tile: tileName}
	step.Rect = getSizesForTile(step.Spritesheet.Layer, step.Spritesheet.Tile)

	return step
}

func getSizesForTile(layerName, tileName string) sdl.Rect {
	var sprite Sprite
	if layerName == "landscapes" {
		sprite = GetBaseLayerSpriteImage()
	} else if layerName == "buildings" {
		sprite = GetBuildingsLayerSpriteImage()
	} else if layerName == "cars" {
		sprite = GetCarsLayerSpriteImage()
	} else if layerName == "city" {
		sprite = GetCityLayerSpriteImage()
	} else if layerName == "animations" {
		sprite = GetAnimationsLayerSpriteImage()
	} else {
		panic("Wrong sprite")
	}

	for _, t := range sprite.Descriptor.Tiles {
		if t.Name == tileName {
			return sdl.Rect{
				X: t.X,
				Y: t.Y,
				W: t.Width,
				H: t.Height,
			}
		}
	}
	panic(fmt.Errorf(fmt.Sprintf("Tile '%v' not found", tileName)))
}
