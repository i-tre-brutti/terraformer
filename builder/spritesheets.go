package builder

import (
	"encoding/json"
	"io/ioutil"

	"github.com/veandco/go-sdl2/img"

	"gitlab.com/i-tre-brutti/common/utils"
)

var sprites Sprites

// InitSprites initializes all the sprites, reading images and their descriptors
func InitSprites(assetsPath string) error {
	sprites.Cars = createSprite("cars", assetsPath)
	sprites.BaseLayer = createSprite("landscapes", assetsPath)
	sprites.BuildingsLayer = createSprite("buildings", assetsPath)
	sprites.CityLayer = createSprite("city", assetsPath)
	sprites.Animations = createSprite("animations", assetsPath)

	return nil
}

func GetCarsLayerSpriteImage() Sprite {
	return sprites.Cars
}

func GetCityLayerSpriteImage() Sprite {
	return sprites.CityLayer
}

func GetBuildingsLayerSpriteImage() Sprite {
	return sprites.BuildingsLayer
}

func GetBaseLayerSpriteImage() Sprite {
	return sprites.BaseLayer
}

func GetAnimationsLayerSpriteImage() Sprite {
	return sprites.Animations
}

func createSprite(fileName, assetsPath string) Sprite {
	imgFile, err := img.Load(assetsPath + "/sprites/" + fileName + ".png")
	utils.E(err, 0)

	raw, err := ioutil.ReadFile(assetsPath + "/sprites/" + fileName + ".json")
	utils.E(err, 0)

	var desc SpriteDescriptor
	json.Unmarshal(raw, &desc)
	sprite := Sprite{Image: imgFile, Descriptor: desc}
	return sprite
}
