package builder

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Sprites contains the available sprites
type Sprites struct {
	BaseLayer      Sprite
	BuildingsLayer Sprite
	CityLayer      Sprite
	Cars           Sprite
	Animations     Sprite
}

// Sprite is the struct describing a sprite. It has an image and its descriptor
type Sprite struct {
	Image      *sdl.Surface
	Descriptor SpriteDescriptor
}

// SpriteDescriptor is a struct containing the values from the sprite json. It contains
// a list of tiles
type SpriteDescriptor struct {
	Tiles []TileDescriptor `json:"tiles"`
}

// TileDescriptor is the descriptor of the tile, needed to load the correct image from the
// spritesheet and draw it
type TileDescriptor struct {
	Name   string `json:"name"`
	X      int32  `json:"x"`
	Y      int32  `json:"y"`
	Width  int32  `json:"width"`
	Height int32  `json:"height"`
}
