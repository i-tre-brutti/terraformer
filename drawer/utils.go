package drawer

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/terraformer/builder"
)

// Window is the page of the app
type Window struct {
	Width  int
	Height int
}

var window Window

// InitTerraformer initializes the app
func InitTerraformer(assetsPath string, fullScreen bool) error {
	err := sdl.VideoInit("")
	if err != nil {
		return err
	}
	var displayMode sdl.DisplayMode
	err = sdl.GetCurrentDisplayMode(0, &displayMode)
	if err != nil {
		return err
	}
	w := int(displayMode.W * 95 / 100)
	h := int(displayMode.H * 90 / 100)
	if fullScreen {
		w = int(displayMode.W)
		h = int(displayMode.H)
	}
	window = Window{w, h}
	// setupWorldMatrix()
	err = builder.InitSprites(assetsPath)
	if err != nil {
		return err
	}
	return nil
}

// GetWindow returns the window
func GetWindow() Window {
	return window
}
