package drawer

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/builder"
)

func RenderVehicle(renderer *sdl.Renderer, camera *CameraState, i int, j int, vehicle *blueprint.Vehicle) {
	x, y, isInsideViewport := camera.MatrixPoint2CameraPixel(i, j)
	if !isInsideViewport {
		return
	}

	buildInstructions := builder.GetElement(vehicle.ID)
	images := getFromCache(renderer, buildInstructions)
	directionFix := directions[vehicle.MovingVector.Direction]
	for _, image := range images.Images {
		src := sdl.Rect{X: 0, Y: 0, W: image.Rect.W, H: image.Rect.H}

		vehicleOffset := directionFix.StartOffset + vehicle.Offset*directionFix.OffsetMultiplier
		pixelOffset := vehicleOffset * directionFix.OffsetMultiplier * BaseNumber * camera.ZoomLevel

		x1 := x + int32(pixelOffset*directionFix.Xsign)

		tileBorderH := int32(float32(image.Rect.H-(image.Rect.W/2)) * camera.ZoomLevel)
		y1 := y - tileBorderH + int32(float32(directionFix.SideFix)*camera.ZoomLevel) + int32((pixelOffset/2)*directionFix.Ysign)

		dstW := int32(float32(image.Rect.W) * camera.ZoomLevel)
		dstH := int32(float32(image.Rect.H) * camera.ZoomLevel)
		dst := sdl.Rect{X: x1, Y: y1, W: dstW, H: dstH}
		err := renderer.Copy(image.Image, &src, &dst)
		utils.E(err, 0)

	}
}

type direction struct {
	StartOffset      float32
	OffsetMultiplier float32
	Xsign            float32
	Ysign            float32
	SideFix          int32
}

var directions = map[blueprint.Dir]*direction{
	blueprint.East: &direction{
		StartOffset:      0,
		OffsetMultiplier: 1,
		Xsign:            1,
		Ysign:            1,
		SideFix:          -BaseNumber/2 + 8,
	},
	blueprint.South: &direction{
		StartOffset:      -1.5,
		OffsetMultiplier: 1,
		Xsign:            -1,
		Ysign:            1,
		SideFix:          BaseNumber/2 - 25,
	},
	blueprint.West: &direction{
		StartOffset:      1,
		OffsetMultiplier: -1,
		Xsign:            -1,
		Ysign:            -1,
		SideFix:          -(BaseNumber/2 + 8),
	},
	blueprint.North: &direction{
		StartOffset:      -0.5,
		OffsetMultiplier: -1,
		Xsign:            1,
		Ysign:            -1,
		SideFix:          BaseNumber/2 - 8,
	},
}
