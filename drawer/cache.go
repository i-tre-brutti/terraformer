package drawer

import (
	"github.com/veandco/go-sdl2/sdl"

	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/builder"
)

type ImagesStruct struct {
	Images []ImageStruct
	W      int32
	H      int32
}
type ImageStruct struct {
	Image *sdl.Texture
	Rect  sdl.Rect
}

var imgCache map[string]*ImagesStruct

func init() {
	if imgCache == nil {
		imgCache = make(map[string]*ImagesStruct)
	}
}

func getFromCache(renderer *sdl.Renderer, buildInstructions builder.BuildInstructions) *ImagesStruct {
	if imgCache[buildInstructions.ID] == nil {
		buildImage(renderer, buildInstructions)
	}

	return imgCache[buildInstructions.ID]
}

func buildImage(renderer *sdl.Renderer, buildInstructions builder.BuildInstructions) {
	var finalH int32
	var maxWidth int32
	var result ImagesStruct

	for _, step := range buildInstructions.Steps {
		// TODO: step per costruire i piani intermedi sono uguali e si puo' implementare una cache

		if step.Rect.W > maxWidth {
			maxWidth = step.Rect.W
		}
		if finalH == 0 {
			finalH = step.Rect.H
		} else {
			finalH += step.Rect.H - (step.Rect.W / 2)
		}

		var image *sdl.Surface

		if step.Spritesheet.Layer == "buildings" {
			image = builder.GetBuildingsLayerSpriteImage().Image
		} else if step.Spritesheet.Layer == "cars" {
			image = builder.GetCarsLayerSpriteImage().Image
		} else if step.Spritesheet.Layer == "city" {
			image = builder.GetCityLayerSpriteImage().Image
		} else if step.Spritesheet.Layer == "animations" {
			image = builder.GetAnimationsLayerSpriteImage().Image
		} else {
			image = builder.GetBaseLayerSpriteImage().Image
		}

		img, err := sdl.CreateRGBSurface(0, step.Rect.W, step.Rect.H, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff)
		utils.E(err, 0)

		srcRect := sdl.Rect{X: 0, Y: 0, W: image.W, H: image.H}

		err = image.Blit(&step.Rect, img, &srcRect)
		utils.E(err, 0)

		imgTexture, err := renderer.CreateTextureFromSurface(img)
		utils.E(err, 0)
		imgStruct := ImageStruct{Image: imgTexture, Rect: step.Rect}
		result.Images = append(result.Images, imgStruct)
	}
	result.H = finalH
	result.W = maxWidth

	imgCache[buildInstructions.ID] = &result
}
