package drawer

import (
	"testing"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

func TestRemapOrientation(t *testing.T) {
	Orientation = blueprint.South
	manifest := [][]string{
		{"T:0:0:0", "T:0:0:2"},
		{"T:0:0:1", "T:0:0:3"},
		{"T:0:0:2", "T:0:0:0"},
		{"T:0:0:3", "T:0:0:1"},
	}

	for _, m := range manifest {
		res := RemapOrientation(m[0])
		if res != m[1] {
			t.Errorf("Wrong remap for %s with result %s, expected %s", m[0], res, m[1])
		}
	}

	Orientation = blueprint.West
	manifest = [][]string{
		{"T:0:0:0", "T:0:0:1"},
		{"T:0:0:1", "T:0:0:2"},
		{"T:0:0:2", "T:0:0:3"},
		{"T:0:0:3", "T:0:0:0"},
	}

	for _, m := range manifest {
		res := RemapOrientation(m[0])
		if res != m[1] {
			t.Errorf("Wrong remap for %s with result %s, expected %s", m[0], res, m[1])
		}
	}

	Orientation = blueprint.East
	manifest = [][]string{
		{"T:0:0:0", "T:0:0:3"},
		{"T:0:0:1", "T:0:0:0"},
		{"T:0:0:2", "T:0:0:1"},
		{"T:0:0:3", "T:0:0:2"},
	}

	for _, m := range manifest {
		res := RemapOrientation(m[0])
		if res != m[1] {
			t.Errorf("Wrong remap for %s with result %s, expected %s", m[0], res, m[1])
		}
	}

	Orientation = blueprint.North
	manifest = [][]string{
		{"T:0:0:0", "T:0:0:0"},
		{"T:0:0:1", "T:0:0:1"},
		{"T:0:0:2", "T:0:0:2"},
		{"T:0:0:3", "T:0:0:3"},
	}

	for _, m := range manifest {
		res := RemapOrientation(m[0])
		if res != m[1] {
			t.Errorf("Wrong remap for %s with result %s, expected %s", m[0], res, m[1])
		}
	}
}
