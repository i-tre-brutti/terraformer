package drawer

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/builder"
)

var textures map[string]*sdl.Texture

// RenderTile draws a tile with given key to position i, j of the screen
func RenderTile(renderer *sdl.Renderer, camera *CameraState, i, j int, key string) {
	x, y, isInsideViewport := camera.MatrixPoint2CameraPixel(i, j)
	if !isInsideViewport {
		return
	}

	buildInstructions := builder.GetElement(key)
	images := getFromCache(renderer, buildInstructions)
	var heightSoFar int32
	for _, image := range images.Images {
		src := sdl.Rect{X: 0, Y: 0, W: image.Rect.W, H: image.Rect.H}
		x1 := x + int32(float32((images.W-image.Rect.W)/2)*camera.ZoomLevel)
		tileBorderH := int32(float32(image.Rect.H-(image.Rect.W/2)) * camera.ZoomLevel)
		y1 := y - tileBorderH - heightSoFar
		dstW := int32(float32(image.Rect.W) * camera.ZoomLevel)
		dstH := int32(float32(image.Rect.H) * camera.ZoomLevel)
		dst := sdl.Rect{X: x1, Y: y1, W: dstW, H: dstH}
		err := renderer.Copy(image.Image, &src, &dst)
		utils.E(err, 0)
		heightSoFar += tileBorderH
	}
}

func scaled(a int32, b float32) int32 {
	return int32(float32(a) * b)
}

// RenderTile draws a tile with given key to position i, j of the screen
func RenderTileGrowStep(renderer *sdl.Renderer, camera *CameraState, i, j int, key string, completion float32) {
	buildInstructions := builder.GetElement(key)

	images := getFromCache(renderer, buildInstructions)
	var heightSoFar int32
	x, y, isInsideViewport := camera.MatrixPoint2CameraPixel(i, j)
	if !isInsideViewport {
		return
	}
	cappedHeight := scaled(images.H, completion*camera.ZoomLevel)
	for _, image := range images.Images {

		nonIsoHeight := image.Rect.H - (image.Rect.W / 2)
		if cappedHeight < heightSoFar {
			// the whole tile should not be drawn...early exit
			return
		}

		leftMargin := (images.W - image.Rect.W) / 2
		x1 := x + scaled(leftMargin, camera.ZoomLevel)
		tileBorderH := scaled(nonIsoHeight, camera.ZoomLevel)
		y1 := y - tileBorderH - heightSoFar
		dstW := scaled(image.Rect.W, camera.ZoomLevel)
		dstH := scaled(image.Rect.H, camera.ZoomLevel)
		src := sdl.Rect{X: 0, Y: 0, W: image.Rect.W, H: image.Rect.H}
		dst := sdl.Rect{X: x1, Y: y1, W: dstW, H: dstH}
		err := renderer.Copy(image.Image, &src, &dst)
		utils.E(err, 0)
		heightSoFar += tileBorderH
	}
}
