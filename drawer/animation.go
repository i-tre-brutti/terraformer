package drawer

import (
	"strings"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/builder"
)

func scale(a int32, s float32) int32 {
	return int32(float32(a) * s)
}

func adaptAnimationToTileSize(key string) (float32, float32) {
	parts := strings.Split(key, ":")
	switch parts[2] {
	case "0":
		// Fire is 64x128, while tile's width is 128
		return 2.0, 2.0
	default:
		return 1.0, 1.0
	}
}

func RenderAnimation(renderer *sdl.Renderer, camera *CameraState, i int, j int, animation *blueprint.Animation) {
	x, y, isInsideViewport := camera.MatrixPoint2CameraPixel(i, j)
	if !isInsideViewport {
		return
	}
	buildInstructions := builder.GetElement(animation.ID)
	images := getFromCache(renderer, buildInstructions)
	renderedFrame := int(float32(len(images.Images)-1) * animation.Progress)
	image := images.Images[renderedFrame]

	src := sdl.Rect{X: 0, Y: 0, W: image.Rect.W, H: image.Rect.H}
	scaleW, scaleH := adaptAnimationToTileSize(animation.ID)
	scaledW := scale(image.Rect.W, scaleW)
	scaledH := scale(image.Rect.H, scaleH)
	dstW := scale(scaledW, camera.ZoomLevel)
	dstH := scale(scaledH, camera.ZoomLevel)
	x1 := x + int32(float32((2*BaseNumber-scaledW)/2)*camera.ZoomLevel)
	y1 := y - dstH + scale(BaseNumber, camera.ZoomLevel)
	dst := sdl.Rect{X: x1, Y: y1, W: dstW, H: dstH}

	err := renderer.Copy(image.Image, &src, &dst)
	utils.E(err, 0)
}
