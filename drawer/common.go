package drawer

import (
	"fmt"
	"strings"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

type CameraState struct {
	ZoomLevel   float32
	TranslateH  int
	TranslateV  int
	Orientation blueprint.Dir
}

func (c *CameraState) Reset() {
	c.ZoomLevel = 0.5
	c.TranslateH = 0
	c.TranslateV = 0
	c.Orientation = blueprint.North
}

/*
	Remap the coordinates (i, j) identifying a cell in the city matrix to
	the window coordinates. It considers how the camera has been moved by the
	user and the zoom level.
*/
func (c *CameraState) MatrixPoint2CameraPixel(i, j int) (int32, int32, bool) {
	scaledSide := int32(float32(BaseNumber) * c.ZoomLevel)
	x := int32(i) * scaledSide
	y := int32(j) * scaledSide
	// convert to iso coordinates
	isoX := y - x
	isoY := (x + y) / 2

	// translation specified by the moved camer
	cameraTranslateH := int32(c.TranslateH+1) * (scaledSide * 2)
	cameraTranslateV := int32(c.TranslateV) * scaledSide

	// translation for the "zero" position: aka map centered on window
	centerTranslateH := int32(GetWindow().Width / 2)
	centerTranslateV := int32(GetWindow().Height/2) - int32(MatrixSize)*scaledSide/2

	finalX := isoX - cameraTranslateH + centerTranslateH
	finalY := isoY + cameraTranslateV + centerTranslateV

	isInsideViewport := true
	if finalX < 0 || finalX+scaledSide*2 > int32(GetWindow().Width) ||
		finalY-scaledSide/2 < 0 || finalY+scaledSide/2 > int32(GetWindow().Height) {
		isInsideViewport = false
	}

	return finalX, finalY, isInsideViewport
}

// AfterRotation returns the new i and j based on actual view of the world
func AfterRotation(i, j int) (int, int) {
	switch Orientation {
	case blueprint.East:
		return j, MatrixSize - 1 - i
	case blueprint.South:
		return MatrixSize - 1 - i, MatrixSize - 1 - j
	case blueprint.West:
		return MatrixSize - 1 - j, i
	default: // must be blueprint.North
		return i, j
	}
}

// RemapOrientation returns an "oriented" ID based on global map orientation
func RemapOrientation(ID string) string {
	parts := strings.Split(ID, ":")
	newOrientation := blueprint.DirFromString(parts[3]).RotateOn(Orientation)
	return fmt.Sprintf("%s:%s:%s:%d", parts[0], parts[1], parts[2], newOrientation)
}

// MatrixSize : Size of the side of the matrix
const MatrixSize = 40

// BaseNumber is the base size of the tile, in pixels
const BaseNumber = 64

// Orientation of the view
var Orientation = blueprint.North
