# Terraformer

## Requirements

### Mac OSX

https://github.com/veandco/go-sdl2/issues/299

```
brew install libvorbis
brew install ffmpeg --with-libvorbis --with-libvpx
brew install sdl2{,_image,_ttf} pkg-config
brew install sdl2_mixer --with-flac --with-fluid-synth --with-libmikmod --with-libmodplug --with-libvorbis --with-smpeg2
go get -v github.com/veandco/go-sdl2/{sdl,mix,img,ttf}
```
