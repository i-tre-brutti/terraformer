package vehicles

import (
	"fmt"
	"math/rand"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/engineer"
)

var carStyles = []string{
	"ambulance",
	"police",
	"taxi",
	"garbage",
	"carBlack1",
	"carBlack2",
	// "carBlack3",
	"carBlack4",
	// "carBlack5",
	// "carBlack6",
	"carGreen1",
	"carGreen2",
	// "carGreen3",
	"carGreen4",
	// "carGreen5",
	"carGreen6",
	// "carBlue1",
	"carBlue2",
	// "carBlue3",
	// "carBlue4",
	// "carBlue5",
	"carBlue6",
	"carRed1",
	"carRed2",
	"carRed3",
	// "carRed4",
	// "carRed5",
	"carRed6",
	// "carSilver1",
	// "carSilver2",
	"carSilver3",
	// "carSilver4",
	"carSilver5",
	"carSilver6",
}

// SpawnVehicles spawns... vehicles...
func SpawnVehicles(city *blueprint.City, vehicles *map[blueprint.OrientedCoord]*blueprint.Vehicle, numberOfCars int) {
	if len(*vehicles) < numberOfCars {
		vector := getAvailableRoad(city)
		if _, isPresent := (*vehicles)[vector]; !isPresent {
			styleIndex := rand.Intn(len(carStyles))
			speed := getSpeed()
			v := blueprint.NewVehicle(
				fmt.Sprintf("C:%s_%s.png", carStyles[styleIndex], vector.Direction.ToString()),
				vector,
				speed)
			(*vehicles)[vector] = &v
		}
	}
}

func getSpeed() float32 {
	return rand.Float32() + 0.4
}

func getAvailableRoad(city *blueprint.City) blueprint.OrientedCoord {
	getRoads := func(startI, deltaI, startJ, deltaJ, max int, startDir blueprint.Dir) []blueprint.OrientedCoord {
		var result []blueprint.OrientedCoord
		for k := 0; k < max; k++ {
			i := startI + k*deltaI
			j := startJ + k*deltaJ
			c := city.Map[i][j]
			if engineer.IsARoad(c) {
				result = append(result, blueprint.OrientedCoord{blueprint.Coord{i, j}, startDir})
			}
		}
		return result
	}

	var roads []blueprint.OrientedCoord
	northBorderRoads := getRoads(0, 0, 0, 1, city.Cols, blueprint.South)
	eastBorderRoads := getRoads(0, 1, city.Cols-1, 0, city.Rows, blueprint.West)
	southBorderRoads := getRoads(city.Rows-1, 0, 0, 1, city.Cols, blueprint.North)
	westBorderRoads := getRoads(0, 1, 0, 0, city.Cols, blueprint.East)
	roads = append(roads, northBorderRoads...)
	roads = append(roads, eastBorderRoads...)
	roads = append(roads, southBorderRoads...)
	roads = append(roads, westBorderRoads...)

	return roads[rand.Intn(len(roads))]
}
