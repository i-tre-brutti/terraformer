package vehicles

import (
	"math/rand"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/engineer"
)

/*
var lastTimestamp time.Time
var lastCount int
var deltaSum float32
var countSinceLast int
*/

var availableDirectionsForRoads = map[string][]int{
	// "(T::(1|10|13)|D::(2|5|6|7|8|9|10|11|12)):0": []int{0, 2}, // straight road & bridges
	// "(T::(1|10|13)|D::(2|5|6|7|8|9|10|11|12)):1": []int{1, 3},
	// "(D|T)::14:(0|2)":                            []int{0, 2}, // closed roads
	// "(D|T)::14:(1|3)":                            []int{1, 3},
	"(T::2:0|D::(3|4):0)": []int{0, 1, 2, 3}, // 4-way crossroad
	"(T|D)::15:0":         []int{1, 2, 3},    // T-shape crossroad
	"(T|D)::15:1":         []int{0, 2, 3},
	"(T|D)::15:2":         []int{0, 1, 3},
	"(T|D)::15:3":         []int{0, 1, 2},
}

func removeBackwardsDir(availableDirs []int, dir int) []int {
	result := make([]int, 0)
	backwardsDir := (dir + 2) % 4
	for _, v := range availableDirs {
		if v == backwardsDir {
			continue
		}
		result = append(result, v)
	}
	return result
}

func MoveVehicles(delta float32, city *blueprint.City, vehicles *map[blueprint.OrientedCoord]*blueprint.Vehicle) {
	/*
		var zeroTime time.Time
		if lastTimestamp == zeroTime {
			lastTimestamp = time.Now()
		} else {
			countSinceLast++
			deltaSum += delta
			if time.Since(lastTimestamp) >= time.Duration(time.Second) {
				lastTimestamp = time.Now()
				lastCount = countSinceLast
				log.Printf("MoveVehicles executed %v times, with average delta of %0.4f", lastCount, deltaSum/float32(lastCount))
				countSinceLast = 0
				deltaSum = 0.0
			}
		}
	*/

	// find a vehicle
	for k, v := range *vehicles {
		v.Advance(delta)
		if !v.IsInsideTile() {
			newVector, err := city.FollowDirection(v.MovingVector)
			i, j := newVector.Position.GetCoords()
			exitedMap := err != nil
			stillOnRoad := city.Contains(i, j, engineer.AvailableRoads)
			if exitedMap || !stillOnRoad {
				delete((*vehicles), k)
				continue
			}

			precedingVehicle, ok := (*vehicles)[newVector]
			if ok {
				// There is already a vehicle occupying our destination tile:
				// just slow down our speed and wait until the new tile is free.
				v.Advance(-delta)
				v.UpdateSpeed(precedingVehicle.CurrentSpeed)
				continue
			}
			// Vechicle is changing tile, remove old reference
			v.RepositionOnTile(newVector)
			var tryDirs []int
			for k, dirs := range availableDirectionsForRoads {
				if city.Contains(i, j, []string{k}) {
					tryDirs = removeBackwardsDir(dirs, int(newVector.Direction))
					break
				}
			}
			if tryDirs != nil && len(tryDirs) > 0 {
				tryDir := blueprint.Dir(tryDirs[rand.Intn(len(tryDirs))])
				_, ok := (*vehicles)[blueprint.OrientedCoord{newVector.Position, tryDir}]
				if !ok {
					v.ChangeDirection(tryDir)
				}
			}
			delete((*vehicles), k)
			(*vehicles)[newVector] = v

		}
		// TODO: detect and manage collisions
	}
}
