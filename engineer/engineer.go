/*
Package engineer transforms the document loaded from MongoDB to a matrix where each spot contains
an identifier.
Its main job is to implement the logic to transform a generic project description to an actual city.
The initial step will be to choose how many buildings to build and how tall they must be.
Each building will also have a proper style. The same 3-floors building could have a different
styles, resulting in different identifiers.
*/
package engineer

import (
	"math/rand"
	"strings"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
	"gitlab.com/i-tre-brutti/terraformer/drawer"
)

// AvailableFreeTiles are tiles IDs identifying empty spaces
var AvailableFreeTiles = []string{
	"T::0:0",
}

// AvailableBaseRoads are tiles IDs identifying roads
var AvailableBaseRoads = []string{
	"T::2:0",           // 4-way crossroad
	"T::1:0", "T::1:1", // straight road
	"T::10:0", "T::10:1", // straight bridges
	"T::14:0", "T::14:1", "T::14:2", "T::14:3", // road end
	"T::15:0", "T::15:1", "T::15:2", "T::15:3", // T-shape crossroad
}

// AvailableCityRoads are tiles IDs identifying fancy roads
// TODO: these variants could be grouped hierarchically based on style which
//		 is now unused
var AvailableCityRoads = []string{
	// straight roads N-S in city and variants
	"D::2:0", "D::5:0", "D::6:0", "D::7:0", "D::8:0", "D::9:0", "D::10:0", "D::11:0", "D::12:0",
	// straight roads W-E in city and variants
	"D::2:1", "D::5:1", "D::6:1", "D::7:1", "D::8:1", "D::9:1", "D::10:1", "D::11:1", "D::12:1",
	// 4-way crossroad in city and variant
	"D::3:0", "D::4:0",
	// brigdes in city N-S and W-E
	"T::13:0", "T::13:1",
	// road ends in city
	"D::14:0", "D::14:1", "D::14:2", "D::14:3",
	// T-shaped crossroad in city
	"D::15:0", "D::15:1", "D::15:2", "D::15:3",
}

// AvailableRoads contains all the IDs for all the roads (base and fancy)
var AvailableRoads = append(AvailableBaseRoads, AvailableCityRoads...)

// AvailableCityDecorations are tiles IDs identifying stand-alone decorations
var AvailableCityDecorations = []string{
	"D::0:0", "D::1:0", "D::13:0", "D::16:0",
}

var AvailableBaseRivers = []string{
	"T::5:0", "T::5:1", "T::5:2", "T::5:3",
}

var AvailableCityRivers = []string{
	"T::6:0", "T::6:1", "T::6:2", "T::6:3",
}

var AvailableRivers = append(AvailableBaseRivers, AvailableCityRivers...)

// mapping plain country-roads to their plain city-roads counterparts
var townRoads = map[string]string{
	"T::1:0":  "D::2:0",
	"T::1:1":  "D::2:1",
	"T::2:0":  "D::3:0",
	"T::14:0": "D::14:0",
	"T::14:1": "D::14:1",
	"T::14:2": "D::14:2",
	"T::14:3": "D::14:3",
	"T::15:0": "D::15:0",
	"T::15:1": "D::15:1",
	"T::15:2": "D::15:2",
	"T::15:3": "D::15:3",
}

// mapping plain city-roads to their fancy counterparts
var townRoadVariants = map[string][]string{
	"D::2:0": []string{"D::5:0", "D::6:0", "D::7:0", "D::8:0", "D::9:0", "D::10:0", "D::11:0", "D::12:0"},
	"D::2:1": []string{"D::5:1", "D::6:1", "D::7:1", "D::8:1", "D::9:1", "D::10:1", "D::11:1", "D::12:1"},
	"D::3:0": []string{"D::4:0"},
}

var cityCache map[string]*blueprint.City

/*
   Return an pseudo-random integer from a string.
*/
func stringToSeed(value string) int64 {
	var result int64 = 0
	for i, v := range value {
		if i%2 == 0 {
			result = result + int64(v) + 1
		} else {
			result = result*int64(v) + 1
		}
	}
	return result
}

/*
	projectSpecificRand creates a new pseudo random number generator basing its seed
	to the internals of `interfaces.ProjectStatistics`.
	The returned PRNG is independet to the previously retuned PRNGs (if any).
	NOTE: every randomness needed while creating the blueprint.City should use this
		  PRNG. This way the city produced will always be the same, unless the ProjectStatistics
		  change. Even in this scenario the changes in the city's blueprint are expected to be
		  incremental if each feature bases its randomness on a new (aka resetted) PRNG.
*/
func projectSpecificRand(data interfaces.ProjectStatistics) *rand.Rand {
	seed := stringToSeed(data.ProjectID)
	return rand.New(rand.NewSource(seed))
}

func getClustersCenters(data interfaces.ProjectStatistics, city *blueprint.City) []blueprint.Coord {
	clustersCount := len(data.ActivityDistribution)
	clustersCenters := make([]blueprint.Coord, clustersCount)
	r := projectSpecificRand(data)
	for i := 0; i < clustersCount; i++ {
		clustersCenters[i] = blueprint.Coord{r.Intn(city.Rows), r.Intn(city.Cols)}
	}
	return clustersCenters
}

func GetVehiclesNumber(data interfaces.ProjectStatistics) int {
	return int(data.Popularity * 100)
}

// BuildCity initializes everything
func BuildCity(data interfaces.ProjectStatistics) *blueprint.City {
	if cityCache == nil {
		cityCache = make(map[string]*blueprint.City)
	}

	city, found := cityCache[data.ProjectID]
	if found {
		return city
	}
	// create a sound default for city: size is taken from drawer.MatrixSize
	// and each cell is filled with T::0:0 (aka plain terrain).
	city = blueprint.NewCity(drawer.MatrixSize, drawer.MatrixSize)
	city.Fill("T::0:0")

	occupiedTiles := 0
	// draw lake only if at least half of the project's file are on the "small side"
	if data.CodeRelativeDiffusion[0]+data.CodeRelativeDiffusion[1] > .5 {
		r := projectSpecificRand(data)
		occupiedTiles += placeLake(r, city)
		if data.CodeRelativeDiffusion[0] > .6 {
			occupiedTiles += placeLake(r, city)
		}
	}
	occupiedTiles += placeRoadsDecumanStyle(city, 3, 1)
	occupiedTiles += placeRiver(projectSpecificRand(data), city)
	// draw tree-lined streets only if the project reaches some quality threshold
	if data.Quality > .2 {
		occupiedTiles += placeDecorations(data, city)
	}
	// to account for the decorations inside the clusters
	occupiedTiles += countInClusterDecorations(data)

	placeBuildings(data, city, occupiedTiles)

	fixRoads(city)
	fixRivers(city)
	placeWoods(projectSpecificRand(data), city)
	cityCache[data.ProjectID] = city
	return city
}

func IsAnyRoad(ID string) bool {
	if IsARoad(ID) {
		return true
	}
	for _, r := range AvailableCityRoads {
		if r == ID {
			return true
		}
	}
	return false
}

// IsARoad returns true if the provided ID identifies a road tile
func IsARoad(ID string) bool {
	for _, r := range AvailableBaseRoads {
		if r == ID {
			return true
		}
	}
	return false
}

// As isARiver but includes bends
func isAnyRiver(ID string) bool {
	river := []string{"T::5:0", "T::5:1", "T::5:2", "T::5:3", "T::6:0", "T::6:1", "T::6:2", "T::6:3"}
	for _, el := range river {
		if el == ID {
			return true
		}
	}
	return false
}

func isARiver(ID string) bool {
	river := []string{"T::5:0", "T::5:1", "T::5:2", "T::5:3"}
	for _, el := range river {
		if el == ID {
			return true
		}
	}
	return false
}

func isABuilding(ID string) bool {
	parts := strings.Split(ID, ":")
	return string(parts[0]) == "B"
}

// identifies a river in the city
func isInTownRiver(city *blueprint.City, i, j int) bool {
	_, _, found := city.FindNextTileNear(i, j, []string{"B:.*", "D::(0|1|13):.*"}, 2)
	return found
}

func isInTown(city *blueprint.City, i, j int) bool {
	// matching both buildings and stand-alone decorations (since these decorations
	// are only found among the buildings).
	_, _, found := city.FindNextTileNear(i, j, []string{"B:.*", "D::(0|1|13):.*"}, 1)
	if found {
		return true
	}
	// if both tiles in opposite directions are city-roads, you're in town
	// FIXME: are we sure that all the roads are already "decorated"?
	return city.IsTileBetween(i, j, AvailableCityRoads[:], true, true)
}

func isCityRoad(city *blueprint.City, i, j int) bool {
	if !city.AreCoordinatesValid(i, j) {
		return false
	}
	ID := city.Map[i][j]
	parts := strings.Split(ID, ":")
	return string(parts[0]) == "D"
}
