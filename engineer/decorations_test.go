package engineer

import (
	"math/rand"
	"testing"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

func TestAdvanceRiverSlimPath(t *testing.T) {
	city := blueprint.NewCity(10, 10)
	city.Fill("blocked")
	type SimplePathInstruction struct {
		start blueprint.Coord
		dir   int
		len   int
	}
	slimPath := make([]SimplePathInstruction, 5)
	slimPath[0] = SimplePathInstruction{blueprint.Coord{3, -1}, 1, 3}
	slimPath[1] = SimplePathInstruction{blueprint.Coord{3, 2}, 2, 2}
	slimPath[2] = SimplePathInstruction{blueprint.Coord{5, 2}, 1, 3}
	slimPath[3] = SimplePathInstruction{blueprint.Coord{5, 5}, 0, 5}
	slimPath[4] = SimplePathInstruction{blueprint.Coord{0, 5}, 1, 4}

	freeTile := "T::0:0"
	for _, v := range slimPath {
		c := v.start
		var err error
		for i := 0; i < v.len; i++ {
			c, err = city.Move(c, v.dir)
			if err != nil {
				t.Error("cannot create path outsied of map")
			}
			city.Map[c.Row][c.Col] = freeTile
		}
	}
	t.Log("\n", city.ToString())
	seed := stringToSeed("TEST")
	r := rand.New(rand.NewSource(seed))
	len := advanceRiver(blueprint.Coord{3, 0}, 1, r, city)
	if len == 0 {
		t.Error("the river should have found its course.")
	}
	t.Log("\n", city.ToString())
}

func TestAdvanceRiverIgnore1TileObstacle(t *testing.T) {
	freeTile := "T::0:0"
	city := blueprint.NewCity(10, 10)
	city.Fill(freeTile)
	for i := 0; i < 1; i++ {
		for row := 0; row < city.Rows; row++ {
			city.Map[row][3+i] = "blocked"
		}
	}

	t.Log("\n", city.ToString())
	seed := stringToSeed("TEST")
	r := rand.New(rand.NewSource(seed))
	len := advanceRiver(blueprint.Coord{3, 0}, 1, r, city)
	if len == 0 {
		t.Error("the river should have found its course.")
	}
	t.Log("\n", city.ToString())
}

func TestAdvanceRiverFindSmallPassage(t *testing.T) {
	freeTile := "T::0:0"
	city := blueprint.NewCity(10, 10)
	city.Fill(freeTile)
	for i := 0; i < 3; i++ {
		for row := 0; row < city.Rows; row++ {
			city.Map[row][3+i] = "blocked"
		}
	}
	city.Map[0][3] = freeTile
	city.Map[0][4] = freeTile
	city.Map[0][5] = freeTile
	city.Map[2][3] = freeTile
	city.Map[2][4] = freeTile
	city.Map[2][5] = freeTile
	city.Map[8][3] = freeTile
	city.Map[8][4] = freeTile
	city.Map[8][5] = freeTile

	t.Log("\n", city.ToString())
	seed := stringToSeed("TEST")
	r := rand.New(rand.NewSource(seed))
	len := advanceRiver(blueprint.Coord{3, 0}, 1, r, city)
	if len == 0 {
		t.Error("the river should have found its course.")
	}
	t.Log("\n", city.ToString())
}

func TestAdvanceRiverNoContiguousPath(t *testing.T) {
	freeTile := "T::0:0"
	city := blueprint.NewCity(10, 10)
	city.Fill(freeTile)
	for i := 0; i < 3; i++ {
		for row := 0; row < city.Rows; row++ {
			city.Map[row][3+i] = "blocked"
		}
	}
	t.Log("\n", city.ToString())
	seed := stringToSeed("TEST")
	r := rand.New(rand.NewSource(seed))
	len := advanceRiver(blueprint.Coord{3, 0}, 1, r, city)
	if len != 0 {
		t.Error("the river should not have found its course.")
	}
	t.Log("\n", city.ToString())
}
