package engineer

import (
	"fmt"
	"log"
	"math/rand"
	"strings"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

// tuneStride tries to follow the specified direction for `stride` tiles ignoring at most
// `maxBlocking` contiguous already-occupied tiles. It returns the position of the last
// inspected tile which is free.
// Important: it assumes `start` to be an empty tile
func tuneStride(city *blueprint.City, start blueprint.Coord, dir int, stride int, maxBlocking int) int {
	tunedStride := 0
	previousBlockingSize := 0
	for i := 0; i < stride; i++ {
		next, err := city.Move(start, dir)
		if err != nil {
			break
		}
		if city.Contains(next.Row, next.Col, AvailableFreeTiles) {
			tunedStride = i + 1
			previousBlockingSize = 0
		} else {
			previousBlockingSize++
			if previousBlockingSize > maxBlocking {
				break
			}
		}
		start = next
	}
	return tunedStride
}

func moveN(city *blueprint.City, c blueprint.Coord, dir int, n int) blueprint.Coord {
	for i := 0; i < n; i++ {
		c, _ = city.Move(c, dir)
	}
	return c
}

// advanceRiver recursively create a river, backtracking if it ends up on a dead end.
// `start` is assumed to be a free tile.
// `originDir` is the direction of the previous segment.
// Note: there must exist a free path joining the west with the east border of the city,
//       otherwise no river is created.
func advanceRiver(start blueprint.Coord, originDir int, r *rand.Rand, city *blueprint.City) int {
	// outside east border: objective reached!
	if start.Col >= city.Cols-1 {
		city.Map[start.Row][start.Col] = fmt.Sprintf("T::5:%d", originDir)
		return 1
	}
	// set of directions towards which the rived can bend
	availableDirs := []int{0, 1, 2}
	removeDir := func(toRemove int) {
		result := make([]int, 0)
		for _, v := range availableDirs {
			if v != toRemove {
				result = append(result, v)
			}
		}
		availableDirs = result
	}
	// we cannot go backward, remove the opposite direction from the set of availableDirs
	removeDir((originDir + 2) % 4)
	// select the (max) advancement of the river for this recursion
	idealStride := r.Intn(4) + 1
	// let's try all the available directions until one leads to the east border!
	for len(availableDirs) > 0 {
		pickDir := r.Intn(len(availableDirs))
		tryDir := availableDirs[pickDir]
		// let's try all the possible stride sizes: we want to be able to find any
		// gap among the pre-existing objects.
		for stride := idealStride; stride > 0; stride-- {
			tunedStride := tuneStride(city, start, tryDir, stride, 1)
			// to be sure that the block is not just one road and the original stride is 1
			if tunedStride == 0 && stride == 1 {
				tunedStride = tuneStride(city, start, tryDir, 2, 1)
			}
			stride = tunedStride
			if stride > 0 {
				endTile := moveN(city, start, tryDir, stride)
				result := advanceRiver(endTile, tryDir, r, city)
				// result > 0 => the river reached the east border. All tiles
				// from endTile to the end are already placed.
				if result > 0 {
					bendingTiles := map[string]string{
						"00": "T::5:0",
						"01": "T::6:1",
						"10": "T::6:3",
						"11": "T::5:1",
						"12": "T::6:2",
						"21": "T::6:0",
						"22": "T::5:0",
					}
					c := start
					// first tile is the junction with the previous stride: we know
					// the old direction and the new one: we decide the junction tile
					city.Map[c.Row][c.Col] = bendingTiles[fmt.Sprintf("%d%d", originDir, tryDir)]
					c, _ = city.Move(c, tryDir)
					// all other tiles are just straight tiles
					for i := 1; i < stride; i++ {
						city.Map[c.Row][c.Col] = fmt.Sprintf("T::5:%d", tryDir)
						c, _ = city.Move(c, tryDir)
					}
					return result + stride
				}
			}
		}
		// the selected direction lead to a dead end. Try with another direction.
		removeDir(tryDir)
	}
	// flag value telling that originDir leads to a dead end
	return 0
}

// placeRiver creates a river while avoiding pre-existings objects.
// The river starts from the west side of the map and ends when it exits
// the east border.
// It tries to avoid pre-existing objects, except roads: it can pass
// under a road as long as the next tile is not another road.
// Note: there must be a free tile on the east border!
func placeRiver(r *rand.Rand, city *blueprint.City) int {
	// Start river on a free tile. Mainly to avoid to spawn the
	// river along a road or above a lake.
	row := r.Intn(city.Rows)
	for i := 0; !city.Contains(row, 0, AvailableFreeTiles); i++ {
		row = (row + 1) % city.Rows
		if i+1 == city.Rows {
			log.Printf("WARNING: no free tiles on east border to start a river!")
			return 0
		}
	}
	current := blueprint.Coord{row, 0}

	count := advanceRiver(current, 1, r, city)
	return count - 1
}

func fixRivers(city *blueprint.City) {
	for i := 0; i < city.Rows; i++ {
		for j := 0; j < city.Cols; j++ {
			if !(isAnyRiver(city.Map[i][j])) {
				continue
			}

			// replace river in town
			if isInTown(city, i, j) || isInTownRiver(city, i, j) {
				parts := strings.Split(city.Map[i][j], ":")
				if parts[0] == "T" && parts[2] == "6" {
					parts[2] = "11"
					city.Map[i][j] = strings.Join(parts, ":")
				} else if parts[0] == "T" && parts[2] == "5" {
					parts[2] = "12"
					city.Map[i][j] = strings.Join(parts, ":")
				}
			}
		}
	}
}

/*
create a random lake with a random shape

There are some constraints in the lake shape: the available sprites
assume that there won't be two opposite shores in the same tile.
FIXME: for now just draw a rectangle shaped lake.
*/
func placeLake(r *rand.Rand, city *blueprint.City) int {
	count := 0
	height := r.Intn(4) + 1
	width := r.Intn(7) + 1
	topLeftI := r.Intn(city.Rows) - width
	topLeftJ := r.Intn(city.Cols) - height
	for i := -1; i < 2*height+1; i++ {
		for j := -1; j < 2*width+1; j++ {
			if !city.AreCoordinatesValid(topLeftI+i, topLeftJ+j) {
				continue
			}
			previousTile := city.At(topLeftI+i, topLeftJ+j)
			tile := "T::9:0" // water tile
			if i == -1 || j == -1 || i == 2*height || j == 2*width {
				// add some wood around the lake
				tile = "T::3:0"
			} else if i == 0 {
				tile = "T::7:0"
				if j == 0 {
					tile = "T::8:3"
				} else if j == 2*width-1 {
					tile = "T::8:0"
				}
			} else if i == 2*height-1 {
				tile = "T::7:2"
				if j == 0 {
					tile = "T::8:2"
				} else if j == 2*width-1 {
					tile = "T::8:1"
				}
			} else if j == 0 {
				tile = "T::7:3"
			} else if j == 2*width-1 {
				tile = "T::7:1"
			}
			city.Map[topLeftI+i][topLeftJ+j] = tile
			if previousTile == "T::0:0" {
				// if the previous tile was already occupied by another tile
				// (most probably another decoration) then we have already counted
				// this tile as "occupied": do not consider it again.
				count++
			}
		}
	}
	return count
}

func addTreeIfPossible(i, j int, city *blueprint.City) int {
	if city.Contains(i, j, AvailableFreeTiles) {
		city.Map[i][j] = "D::0:0"
		return 1
	}
	return 0
}

/*
	Place some decorations

	For now, we just put some trees along the roads near the center of the buildings clusters.
	NOTE: the logic for placing trees works only with roads in decuman style.
*/
func placeDecorations(data interfaces.ProjectStatistics, city *blueprint.City) int {
	count := 0
	r := projectSpecificRand(data)
	clustersCenters := getClustersCenters(data, city)
	for _, center := range clustersCenters {
		i, j := center.GetCoords()
		decorationsLength := r.Intn(5)
		decorationOrientation := i % 2 // 0 -> vertical, 1 -> horizontal
		startI, startJ, found := city.FindNextTileNear(i, j, AvailableBaseRoads[:], 8)
		if !found {
			continue
		}
		for w := 0; w < decorationsLength; w++ {
			road := city.At(startI, startJ)
			if road == AvailableBaseRoads[1] {
				decorationOrientation = 0
			} else if road == AvailableBaseRoads[2] {
				decorationOrientation = 1
			}
			if decorationOrientation == 0 {
				count += addTreeIfPossible(startI+w, startJ-1, city)
				count += addTreeIfPossible(startI+w, startJ+1, city)
				count += addTreeIfPossible(startI-w, startJ-1, city)
				count += addTreeIfPossible(startI-w, startJ+1, city)
			}
			if decorationOrientation == 1 {
				count += addTreeIfPossible(startI-1, startJ+w, city)
				count += addTreeIfPossible(startI+1, startJ+w, city)
				count += addTreeIfPossible(startI-1, startJ-w, city)
				count += addTreeIfPossible(startI+1, startJ-w, city)
			}
		}
	}
	return count
}

// placeWoods places wood-tiles on empty tiles with higher probability near the borders of the city
func placeWoods(r *rand.Rand, city *blueprint.City) int {
	count := 0
	centerRow := city.Rows / 2
	centerCol := city.Cols / 2
	abs := func(a int) int {
		if a < 0 {
			return -a
		}
		return a
	}
	for row := 0; row < city.Rows; row++ {
		for col := 0; col < city.Cols; col++ {
			if city.At(row, col) == "T::0:0" {
				distFromCenter := abs(row-centerRow) + abs(col-centerCol) + 1
				prob := float32(distFromCenter) / float32(centerRow+centerCol+2)
				prob = prob * prob
				if rand.Float32() < prob {
					city.Map[row][col] = "T::3:0"
					count++
				}
			}
		}
	}
	return count
}
