package engineer

import "testing"

func TestShouldDrawRoad(t *testing.T) {
	manifest := [][]int{
		{0, 0, 2, 0, 1},
		{0, 0, 2, 1, 0},
		{1, 0, 2, 1, 1},
		{0, 0, 3, 1, 0},
		{1, 1, 3, 1, 1},
		{0, 1, 3, 1, 1},
		{3, 3, 3, 1, 0},
	}
	for _, m := range manifest {
		if shouldDrawRoad(m[0], m[1], m[2], m[3]) != (m[4] == 1) {
			t.Errorf("Wrong check %v", m)
		}
	}
}

func TestRoadByPosition(t *testing.T) {
	type Manifest struct {
		x   int
		y   int
		mod int
		v   int
		res string
	}
	manifest := []Manifest{
		{0, 0, 2, 0, "T::2:0"},
		{1, 0, 2, 1, "T::1:1"},
		{1, 1, 3, 1, "T::2:0"},
		{0, 1, 3, 1, "T::1:0"},
	}

	for _, m := range manifest {
		res := roadByPosition(m.x, m.y, m.mod, m.v)
		if m.res != res {
			t.Errorf("Wrong check %v, %s", m, res)
		}
	}
}
