package engineer

import (
	"fmt"
	"math"
	"math/rand"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

// MakeRandomCity builds a decuman-style-road city full or random buildings
func MakeRandomCity(rows, cols int) (*blueprint.City, error) {
	city := blueprint.NewCity(rows, cols)
	city.Fill("T::0:0")

	placeRoadsDecumanStyle(city, 4, 2)
	for i := 0; i < city.Rows; i++ {
		for j := 0; j < city.Cols; j++ {
			if city.At(i, j) == "T::0:0" {
				height := rand.Intn(5) + 1
				orientation := rand.Intn(4)
				// We know that thera are at most 14 styles for buildings, so we hardcode it!
				style := rand.Intn(14)
				city.Map[i][j] = fmt.Sprintf("B:%d:%d:%d", height, style, orientation)
			}
		}
	}
	return city, nil
}

// MakeAsymmetricCity builds a city where buildings are shaped as an "L"
// so that it is easy to check for inconsistency with orientations, etc.
// Note: we assume `rows` and `cols` >= 2
func MakeAsymmetricCity(rows, cols int) (*blueprint.City, error) {
	city := blueprint.NewCity(rows, cols)
	city.Fill("T::0:0")

	placeRoadsDecumanStyle(city, 4, 2)

	for j := 1; j < city.Cols; j++ {
		if city.At(1, j) == "T::0:0" {
			city.Map[1][j] = fmt.Sprintf("B:%d:%d:0", int(math.Min(float64(j), 7.0)), j%14)
		}
	}
	for i := 1; i < city.Rows/2; i++ {
		if city.At(i, 1) == "T::0:0" {
			city.Map[i][1] = fmt.Sprintf("B:%d:%d:0", int(math.Min(float64(i), 4.0)), i%14)
		}
	}

	return city, nil
}

func MakeCityDebugCars(rows, cols int) (*blueprint.City, error) {
	var city *blueprint.City

	city = blueprint.NewCity(9, 9)
	city.Fill("T::0:0")
	placeRoadsDecumanStyle(city, 3, 1)

	city.Map[4][0] = "T::5:1"
	city.Map[4][1] = "T::5:1"
	city.Map[4][2] = "T::5:1"
	city.Map[4][3] = "T::5:1"
	city.Map[4][4] = "T::5:1"
	city.Map[4][5] = "T::5:1"
	city.Map[4][6] = "T::6:3"
	city.Map[3][6] = "T::6:2"
	city.Map[3][5] = "T::5:1"
	city.Map[3][4] = "T::5:1"
	city.Map[3][3] = "T::6:0"
	city.Map[2][3] = "T::6:1"
	city.Map[2][4] = "T::5:1"
	city.Map[2][5] = "T::5:1"
	city.Map[2][6] = "T::5:1"
	city.Map[2][7] = "T::5:1"
	city.Map[2][8] = "T::5:1"

	city.Map[1][3] = "D::0:0"

	fixRoads(city)
	fixRivers(city)

	return city, nil
}
