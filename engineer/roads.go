package engineer

import (
	"fmt"
	"log"
	"math/rand"

	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

func shouldDrawRoad(x, y, mod, v int) bool {
	return x%mod == v || y%mod == v
}

func roadByPosition(x, y, mod, v int) string {
	var ret string
	if x%mod == v && y%mod == v {
		ret = AvailableBaseRoads[0]
	} else if y%mod == v {
		ret = AvailableBaseRoads[1]
	} else {
		// x%mod == v
		ret = AvailableBaseRoads[2]
	}
	return ret
}

// placeRoadsDecumanStyle creates roads such to create sqare block of empty terraing of size `blockSize`.
// The parameter `offset` specifies how many tiles to skip before placing the first road.
func placeRoadsDecumanStyle(city *blueprint.City, blockSize int, offset int) int {
	count := 0
	for i := 0; i < city.Rows; i++ {
		for j := 0; j < city.Cols; j++ {
			if shouldDrawRoad(i, j, blockSize+1, offset) {
				if city.Contains(i, j, AvailableFreeTiles) {
					city.Map[i][j] = roadByPosition(i, j, blockSize+1, offset)
					count++
				}
			}
		}
	}
	return count
}

func checkBridgeInADirection(city *blueprint.City, coord blueprint.Coord, dir blueprint.Dir) bool {
	ret := false
	for {
		var err error
		coord, err = city.Move(coord, int(dir))
		if err != nil {
			// out of border: coordinate is not inside city
			break
		}

		i, j := coord.GetCoords()
		if city.Contains(i, j, AvailableRoads) {
			return true
		} else if city.Contains(i, j, AvailableRivers) {
			continue
		} else {
			break
		}

	}
	return ret
}

func shouldAddBridgeOnI(city *blueprint.City, i, j int) bool {
	c := blueprint.Coord{i, j}
	return checkBridgeInADirection(city, c, blueprint.South) && checkBridgeInADirection(city, c, blueprint.North)
}

func shouldAddBridgeOnJ(city *blueprint.City, i, j int) bool {
	c := blueprint.Coord{i, j}
	return checkBridgeInADirection(city, c, blueprint.West) && checkBridgeInADirection(city, c, blueprint.East)
}

func addBridgeOverRiver(city *blueprint.City, i, j int) {
	if isAnyRiver(city.Map[i][j]) {
		// check if we need a bridge in a direction
		northBridgeNeeded := false
		southBridgeNeeded := false
		westBridgeNeeded := false
		eastBridgeNeeded := false

		if !city.AreCoordinatesValid(i-1, j) || IsAnyRoad(city.Map[i-1][j]) {
			northBridgeNeeded = true
		}

		if !city.AreCoordinatesValid(i+1, j) || IsAnyRoad(city.Map[i+1][j]) {
			southBridgeNeeded = true
		}

		if !city.AreCoordinatesValid(i, j-1) || IsAnyRoad(city.Map[i][j-1]) {
			westBridgeNeeded = true
		}

		if !city.AreCoordinatesValid(i, j+1) || IsAnyRoad(city.Map[i][j+1]) {
			eastBridgeNeeded = true
		}

		if northBridgeNeeded && southBridgeNeeded && westBridgeNeeded && eastBridgeNeeded {
			// what? This shouldn't happen!
			log.Println("A ")
		} else if northBridgeNeeded && southBridgeNeeded {
			if isInTownRiver(city, i, j) {
				city.Map[i][j] = "T::13:0"
			} else {
				city.Map[i][j] = "T::10:0"
			}
		} else if westBridgeNeeded && eastBridgeNeeded {
			if isInTownRiver(city, i, j) {
				city.Map[i][j] = "T::13:1"
			} else {
				city.Map[i][j] = "T::10:1"
			}
		}
	}
}

// Note: we work with non-road tiles because otherwise our decision to
// terminate a road will be overidden by the next iterations of `fixRoads`.
// Exemple: bridges are added when inspecting a river, the previous road leading
// to the river would find a non-road tile thus terminating the road.
// FIXME: cannot manage a 4-way crossroad becoming a L road or a straight road
func trimAdjacentRoadIfNecessary(city *blueprint.City, i, j int) {
	if city.Contains(i, j, AvailableRoads) {
		return
	}
	currentCoord := blueprint.Coord{i, j}
	for dir := range blueprint.AllDirs() {
		blockedDir := (int(dir) + 2) % 4
		nextCoord, err := city.Move(currentCoord, int(dir))
		if err != nil {
			continue
		}
		i2, j2 := nextCoord.GetCoords()
		// straight roads of the same direction!
		straightPlainRoads := fmt.Sprintf("T::1:%d", int(dir)%2)
		straightFancyRoads := fmt.Sprintf("D::(2|5|6|7|8|9|10|11|12):%d", (int(dir) % 2))
		plainCrossroads := "T::2:0"
		fancyCrossroads := "D::(3|4):0"
		if city.Contains(i2, j2, []string{straightPlainRoads}) {
			city.Map[i2][j2] = fmt.Sprintf("T::14:%d", blockedDir)
		} else if city.Contains(i2, j2, []string{straightFancyRoads}) {
			city.Map[i2][j2] = fmt.Sprintf("D::14:%d", blockedDir)
		} else if city.Contains(i2, j2, []string{plainCrossroads}) {
			city.Map[i2][j2] = fmt.Sprintf("T::15:%d", blockedDir)
		} else if city.Contains(i2, j2, []string{fancyCrossroads}) {
			city.Map[i2][j2] = fmt.Sprintf("D::15:%d", blockedDir)
		}
	}
}

func fixRoads(city *blueprint.City) {
	for i := 0; i < city.Rows; i++ {
		for j := 0; j < city.Cols; j++ {
			if !(shouldDrawRoad(i, j, 4, 1)) {
				continue
			}

			// makeFixesOnRiverWithMultipleRoads(city, i, j)
			addBridgeOverRiver(city, i, j)
			trimAdjacentRoadIfNecessary(city, i, j)

			// Add decorations to roads
			if isInTown(city, i, j) {
				newRoad, ok := townRoads[city.Map[i][j]]
				if ok {
					// variant?
					availableVariants, ok := townRoadVariants[newRoad]
					if ok && rand.Float32() < 0.2 {
						newRoad = availableVariants[rand.Intn(len(availableVariants))]
					}
					city.Map[i][j] = newRoad
				}
			}
		}
	}
}

// This tries to fix the map placing multiple roads over a river. This means that if a river is under
// a road for multiple, straight, tiles, it will be replaced with the tile of the road over the river.
// Note: this is buggy :)
func makeFixesOnRiverWithMultipleRoads(city *blueprint.City, i, j int) {
	if shouldAddBridgeOnI(city, i, j) {
		for i1 := i; i1 >= 0; i1-- {
			if city.AreCoordinatesValid(i1, j) && (IsAnyRoad(city.Map[i1][j]) || !shouldDrawRoad(i1, j, 4, 1)) {
				break
			}
			if isInTown(city, i1, j) {
				city.Map[i1][j] = "T::13:0"
			} else {
				city.Map[i1][j] = "T::10:0"
			}

		}
		for i2 := i; i2 < city.Rows; i2++ {
			if city.AreCoordinatesValid(i2, j) && (IsAnyRoad(city.Map[i2][j]) || !shouldDrawRoad(i2, j, 4, 1)) {
				break
			}
			if isInTown(city, i2, j) {
				city.Map[i2][j] = "T::13:0"
			} else {
				city.Map[i2][j] = "T::10:0"
			}
		}
	}
	if shouldAddBridgeOnJ(city, i, j) {
		for j1 := j; j1 >= 0; j1-- {
			if city.AreCoordinatesValid(i, j1) && (IsAnyRoad(city.Map[i][j1]) || !shouldDrawRoad(i, j1, 4, 1)) {
				break
			}
			if isInTown(city, i, j1) {
				city.Map[i][j1] = "T::13:1"
			} else {
				city.Map[i][j1] = "T::10:1"
			}

		}
		for j2 := j; j2 < city.Rows; j2++ {
			if city.AreCoordinatesValid(i, j2) && (IsAnyRoad(city.Map[i][j2]) || !shouldDrawRoad(i, j2, 4, 1)) {
				break
			}
			if isInTown(city, i, j2) {
				city.Map[i][j2] = "T::13:1"
			} else {
				city.Map[i][j2] = "T::10:1"
			}
		}
	}
}
