package engineer

import (
	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gopkg.in/mgo.v2/bson"
)

func LoadStatistics(projectID string) (interfaces.ProjectStatistics, error) {
	var statistics interfaces.ProjectStatistics

	db.InitSession()
	defer db.CloseSession()

	collection := db.GetStatisticsCollection()
	err := collection.Find(bson.M{"project_id": projectID, "document_version": 1}).One(&statistics)

	return statistics, err
}
