package engineer

import (
	"fmt"
	"log"
	"math/rand"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
	"gitlab.com/i-tre-brutti/terraformer/blueprint"
)

func rIntFrom(r *rand.Rand, data []int) int {
	return data[r.Intn(len(data))]
}
func randomIntFrom(data []int) int {
	return data[rand.Intn(len(data))]
}
func randomStrFrom(data []string) string {
	return data[rand.Intn(len(data))]
}

// computeHeightsForBuildings decides the heights of the buildings based on the supplied ProjectStatistics.
// It returns the list of buildings's heights already randomly shuffled.
// To compute the heights it considers:
// 1. CodeRelativeDiffusion: the base-line heights of the buildings
// 2. CodeAbsoluteDiffusion: a multiplier to increase the base-line height of the buildings
// 3. Expansion: to decide how many buildings must be present in the city
func computeHeightsForBuildings(data interfaces.ProjectStatistics, availableFreeTiles int) []int {
	count := int(float64(availableFreeTiles) * data.Expansion)
	if count > availableFreeTiles {
		count = availableFreeTiles
	}
	buildings := make([]int, count)
	relativePerc := make([]int, len(data.CodeRelativeDiffusion))
	absolutePerc := make([]int, len(data.CodeAbsoluteDiffusion))
	for i := 0; i < len(relativePerc); i++ {
		relativePerc[i] = int(data.CodeRelativeDiffusion[i] * float64(count))
		absolutePerc[i] = int(data.CodeAbsoluteDiffusion[i] * float64(count))
	}
	sumFunc := func(input []int) int {
		result := 0
		for _, item := range input {
			result += item
		}
		return result
	}
	// Floating-point rounding error can lead to an inconsistend number of buildings.
	// We arbitrary decide that the missing buildings/numbers due to these errors are
	// to be added to the lowest level entry.
	// Note: we cannot have a negative diff because `int` conversion rounds to floor.
	if diff := count - sumFunc(relativePerc); diff > 0 {
		relativePerc[0] += diff
	}
	if diff := count - sumFunc(absolutePerc); diff > 0 {
		absolutePerc[0] += diff
	}

	currentIdx := 0
	r := projectSpecificRand(data)
	randIdx := r.Perm(count)
	for height, partialCount := range relativePerc {
		for k := 0; k < partialCount; k++ {
			if currentIdx >= count {
				// this should never happens: sum(i for i in relativePer) < count
				log.Println("MHHHH....1")
				break
			}
			// we use `height+1` because in the next step we use a multiplier to
			// further refine these height. Using 0 leads to completly ignore
			// this next multiplier.
			buildings[randIdx[currentIdx]] = height + 1
			currentIdx++
		}
	}
	currentIdx = 0
	for multiplier, partialCount := range absolutePerc {
		for k := 0; k < partialCount; k++ {
			if currentIdx >= count {
				// this should never happens: sum(i for i in relativePer) < count
				log.Println("MHHHH....2")
				break
			}
			// items in buildins are already randomized, we can inspect them sequentially
			// without the need to randomize again the index.
			buildings[currentIdx] = buildings[currentIdx] * (multiplier + 1)
			currentIdx++
		}
	}

	// we added a +1 to the heights of building to manage the multiplier. We now remove it
	for i := 0; i < len(buildings); i++ {
		buildings[i] = buildings[i] - 1
	}

	return buildings
}

// computeStylesForBuildings decides which of the available styles to use for the given project.
func computeStylesForBuildings(data interfaces.ProjectStatistics) []int {
	maxStyles := 14 // NOTE: must reflect the available style inside assets/styles.json
	stylesCount := int(float64(maxStyles) * data.Contributors)
	if stylesCount == 0 {
		stylesCount = 1
	} else if stylesCount > maxStyles {
		stylesCount = maxStyles
	}
	styles := make([]int, stylesCount)
	r := projectSpecificRand(data)
	for i := 0; i < stylesCount; i++ {
		styles[i] = r.Intn(maxStyles)
	}
	return styles
}

// createIdentifiersForBuildings create the strings that will be saved inside the blueprint.City.Map.
func createIdentifiersForBuildings(r *rand.Rand, buildingsHeights []int, buildingsStyles []int) []string {
	identifiers := make([]string, len(buildingsHeights))
	for i, height := range buildingsHeights {
		style := rIntFrom(r, buildingsStyles)
		identifiers[i] = fmt.Sprintf("B:%d:%d:0", height, style)
	}
	return identifiers
}

func countInClusterDecorations(data interfaces.ProjectStatistics) int {
	return int(50.0 * data.Quality)
}

func createIdentifiersForClusterDecorations(data interfaces.ProjectStatistics) []string {
	count := countInClusterDecorations(data)
	decorations := make([]string, count)
	for i := 0; i < count; i++ {
		decorations[i] = randomStrFrom(AvailableCityDecorations)
	}
	return decorations
}

// findAdjacentRandomRoadOrientation returns a direction that leads to a road starting from the supplied coordinate.
// If there are no roads confining to the supplied coordinate, then returns North.
func findAdjacentRandomRoadOrientation(c blueprint.Coord, city *blueprint.City) int {
	var goodDirs []int
	for dir := 0; dir < 4; dir++ {
		if c, err := city.Move(c, dir); err == nil && city.Contains(c.Row, c.Col, AvailableBaseRoads) {
			goodDirs = append(goodDirs, dir)
		}
	}
	if len(goodDirs) > 0 {
		return randomIntFrom(goodDirs)
	}
	return rand.Intn(4)
}

// placeCluster places the supplied elements in a spyral-wise manner starting
// from the specified center.
// Notes:
// 1. elements should contain strings representing a drawable tile
// 2. the order of elements matter: this function does not perform any randomization
func placeCluster(
	center blueprint.Coord,
	elements []string,
	city *blueprint.City) int {

	placed := 0
	for _, el := range elements {
		row, col, found := city.FindNextTileNear(center.Row, center.Col, AvailableFreeTiles[:], city.Cols)
		if !found {
			fmt.Println("DEBUG: cannot find an available tile to place building for cluster ", center)
			break
		}
		// decide the orientation of the building: if possible face to a road
		// FIXME: face to a road -> face to not-a-building (e.g., fountain, tree, decoration)
		dir := findAdjacentRandomRoadOrientation(blueprint.Coord{row, col}, city)
		identifier := fmt.Sprintf("%s%d", el[:len(el)-1], dir)
		city.Map[row][col] = identifier
		placed++
	}

	if utils.IsDebugEnv() {
		city.Map[center.Row][center.Col] = "T::4:0"
	}

	return placed
}

func sortBuildings(data []string) []string {
	result := make([]string, len(data))
	copy(result, data)
	buildings := make([]string, 0)
	for _, el := range result {
		if strings.Split(el, ":")[0] == "B" {
			buildings = append(buildings, el)
		}
	}
	compare := func(i, j int) bool {
		h1, _ := strconv.ParseInt(strings.Split(buildings[i], ":")[1], 0, 0)
		h2, _ := strconv.ParseInt(strings.Split(buildings[j], ":")[1], 0, 0)
		return h1 > h2
	}
	sort.Slice(buildings, compare)
	index := 0
	for i, el := range result {
		if strings.Split(el, ":")[0] == "B" {
			result[i] = buildings[index]
			index++
		}
	}
	return result
}

func placeBuildings(data interfaces.ProjectStatistics, city *blueprint.City, occupiedTiles int) int {
	var r *rand.Rand
	availableFreeTiles := city.Rows*city.Cols - occupiedTiles
	buildingsHeigths := computeHeightsForBuildings(data, availableFreeTiles)
	buildingsStyles := computeStylesForBuildings(data)
	r = projectSpecificRand(data)
	buildings := createIdentifiersForBuildings(r, buildingsHeigths, buildingsStyles)

	decorations := createIdentifiersForClusterDecorations(data)

	// randomly merge the array of buildings and decorations
	r = projectSpecificRand(data)
	buildingsAndDecorations := make([]string, len(buildings)+len(decorations))
	permutation := r.Perm(len(buildings) + len(decorations))
	for orderedIndex, randomIndex := range permutation {
		element := ""
		if randomIndex < len(buildings) {
			element = buildings[randomIndex]
		} else {
			element = decorations[randomIndex-len(buildings)]
		}
		buildingsAndDecorations[orderedIndex] = element
	}

	// create the clusters
	clustersCenters := getClustersCenters(data, city)
	log.Println("WARNING: sorting buildings by height")
	clustersCount := len(clustersCenters)
	clustersSizes := make([]int, clustersCount)
	surplus := 0.0
	for i, v := range data.ActivityDistribution {
		idealSize := float64(len(buildingsAndDecorations)) * v
		actualSize := int(idealSize)
		surplus += idealSize - float64(actualSize)
		if surplus > 0.99999 {
			surplus -= 1.0
			actualSize++
		}
		clustersSizes[i] = actualSize
	}
	placed := 0
	for i, center := range clustersCenters {
		clusterSize := clustersSizes[i]
		buildingsForCluster := sortBuildings(buildingsAndDecorations[placed : placed+clusterSize])
		placed += placeCluster(center, buildingsForCluster, city)
	}

	return placed
}
